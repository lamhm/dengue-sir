import Mamba
import DataFrames
import CSV
import Dates
import Plots

import DengueSIR


include("util.jl")



function as_dataframe(chains::Mamba.ModelChains, simplifyChains::Bool = true)
    if simplifyChains
        chains = filter_chain_data(chains)
    end

    dataFrame = DataFrames.DataFrame()
    for i = 1 : size(chains, 3)
        DataFrames.append!(
            dataFrame,
            DataFrames.DataFrame(chains.value[:, :, i])
        )
    end

    colNames = chains.names
    for i = 1 : length(colNames)
        tmp = split(colNames[i], r"[\[\]]")
        if length(tmp) > 1
            colNames[i] = tmp[1] * "_" * tmp[2]
        end
    end
    DataFrames.rename!(dataFrame, Symbol.(colNames))

    return dataFrame
end



function redist_age_groups(origDataVec::Vector{Float64},
                           origAgeGroupSizes::Vector{Float64},
                           newAgeGroupSizes::Vector{Float64})::Vector{Float64}

    nOrigAgeGroups = length(origAgeGroupSizes)
    nNewAgeGroups = length(newAgeGroupSizes)
    newDataVec = zeros(Float64, nNewAgeGroups)

    origAgeGroupIdx = 1
    origData = origDataVec[1]
    origAgeGroupSize = origAgeGroupSizes[1]

    newAgeGroupIdx = 1
    newAgeGroupSize = newAgeGroupSizes[1]

    while origAgeGroupIdx <= nOrigAgeGroups && newAgeGroupIdx <= nNewAgeGroups
        if origAgeGroupSize < newAgeGroupSize
            newDataVec[newAgeGroupIdx] += origData
            newAgeGroupSize -= origAgeGroupSize
            origAgeGroupIdx += 1
            if origAgeGroupIdx <= nOrigAgeGroups
                origData = origDataVec[origAgeGroupIdx]
                origAgeGroupSize = origAgeGroupSizes[origAgeGroupIdx]
            end

        else ## origAgeGroupSize >= newAgeGroupSize
            newData = origData * (newAgeGroupSize / origAgeGroupSize)
            newDataVec[newAgeGroupIdx] += newData
            origData -= newData
            origAgeGroupSize -= newAgeGroupSize
            newAgeGroupIdx += 1
            if newAgeGroupIdx <= nNewAgeGroups
                newAgeGroupSize = newAgeGroupSizes[newAgeGroupIdx]
            end
            if origAgeGroupSize == newAgeGroupSize
                origAgeGroupIdx += 1
                if origAgeGroupIdx <= nOrigAgeGroups
                    origData = origDataVec[origAgeGroupIdx]
                    origAgeGroupSize = origAgeGroupSizes[origAgeGroupIdx]
                end
            end
        end
    end

    return newDataVec
end



## Calculate the birth month distribution of the simulated infant cases
function birth_month_dist_of_infants(chains::Mamba.ModelChains;
                                     nSavedSims::Int = 0,
                                     normaliseCountsByBirthRate::Bool = true,
                                     convertCountsToProportions::Bool = true,
                                     relativeToJan::Bool = false
                                     ) :: Matrix{Float64}

    infantAgeGroupSizes = [1/12 for i = 1:24]

    simCaseMatrix, simSizes = load_sim_expect_case_count(chains)
    nSims, nHosps, nSimAgeGroups, nPeriods = size(simCaseMatrix)
    modelVars = chains.model.nodes[:modelVars]
    simAgeGroupSizes = modelVars.ageGroupSizes / modelVars.nDaysPerYear

    if nSavedSims <= 0 || nSavedSims > nSims
        nSavedSims = nSims
    end
    thinningRate = div(nSims, nSavedSims)


    nCasesPerBirthMonth = zeros(Float64, 12, nSavedSims)
    savedSimIdx = 0

    for sim = 1 : thinningRate : nSims
        savedSimIdx += 1
        samplingTimes = 0
        date = Dates.Date(2005, 1, 3)  # First Monday of 2005
        currentMonth = 1
        birthMonthOfGroups = [ mod(i + currentMonth - 1, 12) + 1
                               for i = -1:-1:-24 ]

        for period = 1 : nPeriods
            newMonth = Dates.month(date)
            if newMonth != currentMonth
                samplingTimes = 0
                currentMonth = newMonth
                birthMonthOfGroups = [ mod(i + currentMonth - 1, 12) + 1
                                       for i = -1:-1:-24 ]
            end

            if samplingTimes < 2
                samplingTimes += 1
                for hosp = 1 : nHosps
                    simCaseCounts = redist_age_groups(
                        simCaseMatrix[sim, hosp, :, period],
                        simAgeGroupSizes,
                        infantAgeGroupSizes
                    )
                    for i = 1 : 24
                        nCasesPerBirthMonth[birthMonthOfGroups[i], savedSimIdx] +=
                            simCaseCounts[i]
                    end
                end
            end

            date += Dates.Day(modelVars.nDaysPerSummaryPeriod)
        end
    end

    if normaliseCountsByBirthRate
        nCasesPerBirthMonth ./= modelVars.seasonalBirthRates
    end

    if convertCountsToProportions
        nCasesPerBirthMonth ./= sum(nCasesPerBirthMonth, dims = 1)
    end

    if relativeToJan
        nCasesPerBirthMonth ./= transpose(nCasesPerBirthMonth[1, :])
    end

    return nCasesPerBirthMonth
end



## Load simulated case counts into a data frame
function sim_cases_as_dataframe(chains::Mamba.ModelChains;
                                nSavedSims::Int = 0,
                                outputAgeGroupSizes = [0.5 for i = 1:28]
                                ) :: DataFrames.DataFrame

    simCaseMatrix, simSizes = load_sim_expect_case_count(chains)
    nSims, nHosps, nSimAgeGroups, nPeriods = size(simCaseMatrix)
    modelVars = chains.model.nodes[:modelVars]
    simAgeGroupSizes = modelVars.ageGroupSizes / modelVars.nDaysPerYear

    if nSavedSims <= 0 || nSavedSims > nSims
        nSavedSims = nSims
    end
    thinningRate = div(nSims, nSavedSims)

    outputAges = cumsum(outputAgeGroupSizes)
    nOutputAgeGroups = length(outputAgeGroupSizes)

    dataFrame = DataFrames.DataFrame()
    for sim = 1 : thinningRate : nSims, hosp = 1 : nHosps
        date = Dates.Date(2005, 1, 1)
        for period = 1 : nPeriods
            simCaseCounts = simCaseMatrix[sim, hosp, :, period]
            outputCaseCounts = redist_age_groups(
                simCaseCounts, simAgeGroupSizes, outputAgeGroupSizes
            )
            append!(
                dataFrame,
                DataFrames.DataFrame(
                    sim = [sim for i = 1:nOutputAgeGroups],
                    hospId = [hosp for i = 1:nOutputAgeGroups],
                    age = outputAges,
                    admDate = [string(date) for i = 1:nOutputAgeGroups],
                    nCases = outputCaseCounts
                ),
                cols = :orderequal
            )

            date += Dates.Day(modelVars.nDaysPerSummaryPeriod)
        end
    end

    return dataFrame
end



function sim_yearly_cases_as_dataframe(
        chains::Mamba.ModelChains;
        nSavedSims::Int = 0,
        outputAgeGroupSizes = [0.5 for i = 1:28],
        outputYearGroups = [2005:2006, 2007:2009, 2010:2012, 2013:2015]
        ) :: DataFrames.DataFrame

    simCaseMatrix, simSizes = load_sim_expect_case_count(chains)
    nSims, nHosps, nSimAgeGroups, nPeriods = size(simCaseMatrix)
    modelVars = chains.model.nodes[:modelVars]
    simAgeGroupSizes = modelVars.ageGroupSizes / modelVars.nDaysPerYear

    if nSavedSims <= 0 || nSavedSims > nSims
        nSavedSims = nSims
    end
    thinningRate = div(nSims, nSavedSims)

    outputAges = cumsum(outputAgeGroupSizes)
    nOutputAgeGroups = length(outputAgeGroupSizes)

    dataFrame = DataFrames.DataFrame()
    for sim = 1 : thinningRate : nSims, hosp = 1 : nHosps
        # 2005-01-02 is the 1st Sunday of 2005
        date = Dates.Date(2005, 1, 2) -
               Dates.Day(modelVars.nDaysPerSummaryPeriod)
        currentYearGroupIdx = 1
        simCaseCounts = zeros(Float64, nSimAgeGroups)
        for period = 1 : nPeriods
            date += Dates.Day(modelVars.nDaysPerSummaryPeriod)
            currentYear = Dates.year(date)

            if currentYear in outputYearGroups[currentYearGroupIdx]
                simCaseCounts += simCaseMatrix[sim, hosp, :, period]
            end
            if !(currentYear in outputYearGroups[currentYearGroupIdx]) ||
                    period == nPeriods
                simCaseCounts ./= length(outputYearGroups[currentYearGroupIdx])
                outputCaseCounts = redist_age_groups(
                    simCaseCounts, simAgeGroupSizes, outputAgeGroupSizes
                )
                append!(
                    dataFrame,
                    DataFrames.DataFrame(
                        sim = [sim for i = 1:nOutputAgeGroups],
                        hospId = [hosp for i = 1:nOutputAgeGroups],
                        age = outputAges,
                        admYearPeriod = [
                            string(outputYearGroups[currentYearGroupIdx])
                            for i = 1:nOutputAgeGroups
                        ],
                        nCases = outputCaseCounts
                    ),
                    cols = :orderequal
                )

                simCaseCounts = zeros(Float64, nSimAgeGroups)
                while currentYearGroupIdx < length(outputYearGroups) &&
                      all(currentYear .> outputYearGroups[currentYearGroupIdx])
                    currentYearGroupIdx += 1
                end
                if !(currentYear in outputYearGroups[currentYearGroupIdx])
                    break
                end
            end
        end
    end

    return dataFrame
end



function load_sim_expect_case_count(chains)
    nHospitals = chains.model.nodes[:nHospitals]
    nPeriods   = chains.model.nodes[:nPeriods]
    modelVars  = chains.model.nodes[:modelVars]
    nAgeGroups = length(modelVars.ageGroupSizes)

    nSims, nVariables, nChains = size(chains.value)
    nTotalSims = nSims * nChains

    simCaseMatrix = Array{Float64, 4}(undef, nTotalSims, nHospitals,
                                      nAgeGroups, nPeriods)

    expectCaseCountIdxs =
        filter((idx) -> startswith(chains.names[idx], "expectCaseCounts"),
               [i for i = 1 : nVariables])

    simCount = 0
    for simIdx = 1 : nSims, chainIdx = 1 : nChains
        simCount += 1
        valueSeries = chains.value[simIdx, expectCaseCountIdxs, chainIdx]
        valueIdx = 0
        for period = 1 : nPeriods,
                ageGroup = 1 : nAgeGroups,
                hospIdx = 1 : nHospitals
            valueIdx += 1
            simCaseMatrix[simCount, hospIdx, ageGroup, period] =
                valueSeries[valueIdx]
        end
    end

    dimensions = Dict{Symbol, Int}(
        :nSims      => nTotalSims,
        :nHospitals => nHospitals,
        :nAgeGroups => nAgeGroups,
        :nPeriods   => nPeriods
    )
    return (simCaseMatrix, dimensions)
end



function times_of_case_counts(modelVars::ModelVars,
                              nTimePoints::Int) :: Vector{Float64}
    nPeriodsPerYear = modelVars.nDaysPerYear / modelVars.nDaysPerSummaryPeriod
    times = [(timePointIdx - 1.0) / nPeriodsPerYear for timePointIdx = 1 : nTimePoints]
    times .+= 2005
    return times
end



function plot_sim_over_real(modelVars::ModelVars;
                            realCaseMatrix::Array{Float64, 3},
                            simCaseMatrix::Array{Float64, 4},
                            hospIdx::Int,
                            ageGroups::Vector{Int},
                            nSimLines::Int = 200,
                            nBurnin::Int   = 0,
                            simLineOpacity = 10)
    nSims, nHospitals, nAgeGroups, nTimePoints = size(simCaseMatrix)

    maxAgeOfGroups = cumsum(modelVars.ageGroupSizes) / modelVars.nDaysPerYear
    minAgeOfGroups = vcat(0.0, maxAgeOfGroups[1 : end-1])
    maxAge = round(maximum(maxAgeOfGroups[ageGroups]), digits = 2)
    minAge = round(minimum(minAgeOfGroups[ageGroups]), digits = 2)

    nPeriodsPerYear = modelVars.nDaysPerYear / modelVars.nDaysPerSummaryPeriod
    times = times_of_case_counts(modelVars, nTimePoints)

    p = Plots.plot()

    nKepts = nSims - nBurnin
    stepSize = max(div(nKepts, nSimLines), 1)
    nLines   = max(div(nKepts, stepSize), 1)
    opacity  = min(simLineOpacity / nLines, 1.0)
    for simIdx = (nBurnin + 1) : stepSize : nSims
        simPlotData =
            sum(simCaseMatrix[simIdx, hospIdx, ageGroups, :], dims = 1)[1, :]
        Plots.plot!(p, times, simPlotData,
                    xticks = [i for i = 2005 : 2016],
                    lw = 2, legend = false, tickfont = Plots.font(11),
                    linecolor = Plots.RGBA(253/255, 150/255, 68/255, opacity) )
    end

    realPlotData = map( (x) -> x < 0 ? NaN : x,
                        realCaseMatrix[hospIdx, ageGroups, :] )
    realPlotData = sum(realPlotData, dims = 1)[1, :]
    Plots.plot!(p, times, realPlotData, lw = 3,
                   linecolor = Plots.RGBA(60/255, 175/255, 160/255, 1.0),
                   title = "Hospital $hospIdx | Age group $minAge - $maxAge")

    return p
end



function plot_lpchains(lpchains)
    p = Plots.plot()
    minY = Inf
    maxY = -Inf
    nChains = size(lpchains, 3)
    for chainIdx = 1 : nChains
        plotData = lpchains.value[:, 1, chainIdx]
        minData = minimum(plotData)
        maxData = maximum(plotData)
        minY = floor(min(minY, minData))
        maxY = ceil(max(maxY, maxData))
        step = max(div(maxY - minY, 5), 1)
        Plots.plot!(p, plotData, ylim = (minY, maxY),
                    ticks = [i for i = minY : step : maxY],
                    legend = false, tickfont = Plots.font(12))
    end
    display(p)
end

using DengueSIR
using Mamba
import Plots
import CSV
import Tables
import Statistics

include("util.jl")
include("post_mcmc_util.jl")



## Load chain data
dataFile = "data/ch1_ch2_data.csv"

resultDir = "results/good_1.2"
resultFile = "200416_103243_mcmc_chains_fullCpr_fitSd20"
# resultFile = "200419_152222_mcmc_chains_partialCpr_fitSd20_serotypesSynced"

# resultDir = "results/old"
# resultFile = "200321_063939_mcmc_chains_partialCpr_fitSd20"

chains = read(resultDir * "/" * resultFile * ".cdt", ModelChains)
chains = chains[:, :, [1, 2, 4]]
size(chains)
chains.model.nodes[:fittingStdev]
chains.range.step
chains.range.start
chains.range.stop
# chains = chains[:, :, 3]

@show filter_chain_data(chains).names
chains.model.nodes[:sndSymptomaticProb]


## Sub-sample chain data
# nChains = size(chains, 3)
# f = first(chains)
# l = last(chains)
# s = step(chains)
# newStep = round( Int, (l - f) / s / 1000 * nChains )
#
# subChains = chains[f:newStep:l, :, :]
# write(resultDir * "/" * resultFile * "_sub.cdt", subChains)


## Save chains' data into a CSV file
# CSV.write(resultDir * "/" * resultFile * "_params.csv", as_dataframe(chains))
# CSV.write(
#     resultDir * "/" * resultFile * "_simCaseCounts.csv",
#     sim_cases_as_dataframe(chains, nSavedSims = 1000,
#                            outputAgeGroupSizes = [2.0, 12.0])
# )
# CSV.write(resultDir * "/" * resultFile * "_simYearlyCaseCounts.csv",
#           sim_yearly_cases_as_dataframe(chains, nSavedSims = 1000))
#
# CSV.write(
#     resultDir * "/" * resultFile * "_hospRiskByMonthOfBirth.csv",
#     Tables.table(
#         birth_month_dist_of_infants(
#             chains, nSavedSims = 1000,
#             normaliseCountsByBirthRate = true, convertCountsToProportions = true
#         )
#     )
# )
#
# CSV.write(
#     resultDir * "/" * resultFile * "_caseProportionsByMonthOfBirth.csv",
#     Tables.table(
#         birth_month_dist_of_infants(
#             chains, nSavedSims = 1000,
#             normaliseCountsByBirthRate = false, convertCountsToProportions = true
#         )
#     )
# )


# birthMonthDistOfInfants = birth_month_dist_of_infants(
#     chains, nSavedSims = 1000,
#     normaliseCountsByBirthRate = true,
#     convertCountsToProportions = true,
#     relativeToJan = false
# )
#
# let p = Plots.plot(xticks = [i for i = 1:12], legend = false,
#                    tickfont = Plots.font(11), xlab = "Month of Birth")
#     for i = 1 : 1000
#         Plots.plot!( p, [m for m = 1:12], birthMonthDistOfInfants[:, i],
#                      lw = 1,
#                      linecolor = Plots.RGBA(60/255, 175/255, 160/255, 0.1) )
#     end
#     p
# end



## Load case data
modelVars = chains.model.nodes[:modelVars]
hospData = DengueSIR.load_data_from_csv(dataFile, modelVars)
caseCountMatrix = DengueSIR.as_3d_array(hospData)

simCaseMatrix, simSizes = load_sim_expect_case_count(chains)

ageGroupScales = modelVars.ageGroupSizes ./ modelVars.ageGroupSizes[1]
maxAge = cumsum(modelVars.ageGroupSizes) ./ 364 .* 12
minAge = vcat(0.0, maxAge[1:end-1])
avgAge = (maxAge + minAge) / 2
# standardAGSize = maxAge[1]

times = times_of_case_counts(modelVars, simSizes[:nPeriods])
yearBreaks     = [2005, 2007, 2010, 2013, 2016]
nYearsInPeriod = [ 0.0,  2.0,  3.0,  3.0,  3.0]

caseDataFrame = DataFrames.DataFrame()
for hosp in 1 : simSizes[:nHospitals], ageGroup in 1 : (simSizes[:nAgeGroups]-1)
    hospName = "CH" * string(hosp)
    yearBreakIdx = 2

    realCaseCount = 0.0
    simCaseCount = zeros(Float64, simSizes[:nSims])

    for timeIdx in 1 : simSizes[:nPeriods]
        if times[timeIdx] >= yearBreaks[yearBreakIdx]
            realCaseCount /= nYearsInPeriod[yearBreakIdx] *
                             ageGroupScales[ageGroup]
            simCaseCount ./= nYearsInPeriod[yearBreakIdx] *
                             ageGroupScales[ageGroup]

            yearPeriod = string(yearBreaks[yearBreakIdx - 1]) * " - " *
                         string(yearBreaks[yearBreakIdx] - 1)
            DataFrames.append!(
                caseDataFrame,
                DataFrames.DataFrame(
                    yearPeriod = yearPeriod,
                    hospital = hospName,
                    avgAge = avgAge[ageGroup],
                    minAge = minAge[ageGroup],
                    maxAge = maxAge[ageGroup],
                    realCaseCount = realCaseCount,
                    simCaseCount_500 = Statistics.median(simCaseCount),
                    simCaseCount_025 = Statistics.quantile(simCaseCount, 0.025),
                    simCaseCount_975 = Statistics.quantile(simCaseCount, 0.975),
                    simCaseCount_250 = Statistics.quantile(simCaseCount, 0.250),
                    simCaseCount_750 = Statistics.quantile(simCaseCount, 0.750)
                )
            )
            yearBreakIdx += 1
            realCaseCount = 0.0
            simCaseCount = zeros(Float64, simSizes[:nSims])
        end
        if caseCountMatrix[hosp, ageGroup, timeIdx] > 0
            realCaseCount += caseCountMatrix[hosp, ageGroup, timeIdx]
            for sim in 1 : simSizes[:nSims]
                simCaseCount[sim] += simCaseMatrix[sim, hosp, ageGroup, timeIdx]
            end
        end
    end

    realCaseCount /= nYearsInPeriod[yearBreakIdx] *
                     ageGroupScales[ageGroup]
    simCaseCount ./= nYearsInPeriod[yearBreakIdx] *
                     ageGroupScales[ageGroup]
    yearPeriod = string(yearBreaks[yearBreakIdx - 1]) * " - " *
                 string(yearBreaks[yearBreakIdx] - 1)
    DataFrames.append!(
        caseDataFrame,
        DataFrames.DataFrame(
            yearPeriod = yearPeriod,
            hospital = hospName,
            avgAge = avgAge[ageGroup],
            minAge = minAge[ageGroup],
            maxAge = maxAge[ageGroup],
            realCaseCount = realCaseCount,
            simCaseCount_500 = Statistics.median(simCaseCount),
            simCaseCount_025 = Statistics.quantile(simCaseCount, 0.025),
            simCaseCount_975 = Statistics.quantile(simCaseCount, 0.975),
            simCaseCount_250 = Statistics.quantile(simCaseCount, 0.250),
            simCaseCount_750 = Statistics.quantile(simCaseCount, 0.750)
        )
    )
end


CSV.write(resultDir * "/" * resultFile * "_simCaseCountSummary.csv",
          caseDataFrame)


## Convergence diagnostics
gelmandiag(filter_chain_data(chains), mpsrf=true, transform=true) |> showall
# describe(chains)
# gewekediag(chains)

changerate(filter_chain_data(chains)) |> showall  # Check acceptance rates
summarystats(filter_chain_data(chains)) |> showall



## Plot the likelihood
lpchains = logpdf(chains)
draw(Mamba.plot(lpchains, legend = true), nrow = 2, ncol = 1)

# lpchains_case = logpdf(chains, :caseCountMatrix)
# draw(Mamba.plot(lpchains_case, legend = true), nrow = 2, ncol = 1)

# lpchains_seroprev = logpdf(chains, :seroprevalence)
# draw(Mamba.plot(lpchains_seroprev, legend = true), nrow = 2, ncol = 1)



## Plot simulated cases over the real data
simCaseMatrix, simSizes = load_sim_expect_case_count(chains)

let plots = Vector{Any}(undef, 4)
    for hospIdx = 1 : 2
        plots[hospIdx] = plot_sim_over_real(
            modelVars,
            realCaseMatrix = caseCountMatrix,
            simCaseMatrix  = simCaseMatrix,
            hospIdx        = hospIdx,
            ageGroups      = get_infant_age_groups(modelVars),
            nSimLines      = 200,
            nBurnin        = 0,
            simLineOpacity = 10
        )
        Plots.plot!(plots[hospIdx], ylim = (0, 45))

        plots[hospIdx + 2] = plot_sim_over_real(
            modelVars,
            realCaseMatrix = caseCountMatrix,
            simCaseMatrix  = simCaseMatrix,
            hospIdx        = hospIdx,
            ageGroups      = get_noninfant_age_groups(modelVars),
            nSimLines      = 200,
            nBurnin        = 0,
            simLineOpacity = 10
        )
        Plots.plot!(plots[hospIdx + 2], ylim = (0, 270))
    end

    finalPlot = Plots.plot(plots..., layout = (2, 2), size = (1200, 800),
                           margin = 7mm)
    display(finalPlot)
    Plots.savefig(finalPlot, resultDir * "/" * resultFile * ".pdf")
end

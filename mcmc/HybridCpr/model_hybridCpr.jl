# include("../src/globaldef.jl")

caseModel = Model(
    # Input nodes:
    #   modelVars,            nHospitals,
    #   nPeriods,             recoveryRate,
    #   hospWeights[,],
    #   sndInfectivity,       sndSymptomaticProb
    #   fittingStdev,
    #   sndSerotypeProp,

	seroprevFitPrecision = Stochastic(
		() -> Uniform(1e2, 1e7)
	),

    caseCountMatrix = Stochastic(
        3,
        (modelVars, nHospitals, nPeriods,
		 fittingStdev, expectCaseCounts) -> begin
		 	nAgeGroups = length(modelVars.ageGroupSizes)
            dists = Array{UnivariateDistribution, 3}(undef, nHospitals,
                                                     nAgeGroups, nPeriods)
            for hospIdx = 1 : nHospitals,
                    ageGroup = 1 : nAgeGroups,
                    period = 1 : nPeriods

                if (!isnan(caseCountMatrix[hospIdx, ageGroup, period])
                    && caseCountMatrix[hospIdx, ageGroup, period] >= 0)
                    dists[hospIdx, ageGroup, period] = Normal(
                        expectCaseCounts[hospIdx, ageGroup, period],
                        fittingStdev
                    )
                else
                    dists[hospIdx, ageGroup, period] =
                        FixedValDist{Float}(-999.0)
                end
            end
            return dists
        end,
        false
    ),

    primaryTranRate = Stochastic(
        () -> Uniform(0.25, 1.0)
    ),

    seasonalTranScales_raw = Stochastic(
        1,
        (modelVars) -> UnivariateDistribution[
			Uniform(0.1, 1.9) for i = 1 : (modelVars.nSeasonalTranPeriods - 1)
		]
    ),

    seasonalTranScales = Logical(
        1,
        (seasonalTranScales_raw, modelVars) -> begin
            tranScales = Vector{Float}(undef, modelVars.nSeasonalTranPeriods)
            tranScales[1] = 1.0
			tranScales[2:end] = seasonalTranScales_raw.value
            return tranScales
        end
    ),

	seasonalTranRates = Logical(
		1,
		(seasonalTranScales, primaryTranRate) ->
		 	seasonalTranScales .* primaryTranRate.value
	),

	# hospRate_1 = Stochastic(
	# 	() -> Uniform(1e-5, 0.9)
	# ),
	#
	# hospRate_2_scale = Stochastic(
	# 	() -> Uniform(0.0, 1.0)
	# ),
	#
    # hospRates = Logical(1,
    #     (hospRate_1, hospRate_2_scale) ->
	# 	 	[hospRate_1.value, hospRate_1.value * hospRate_2_scale.value]
    # ),

	hospRate_2 = Stochastic(
		() -> Uniform(1e-5, 0.3)
	),

	hospRate_diff = Stochastic(
		() -> Uniform(0.0, 0.7)
	),

	hospRates = Logical(1,
		(hospRate_2, hospRate_diff) ->
			[hospRate_2.value + hospRate_diff.value, hospRate_2.value]
	),

    adeEffectScale = Stochastic(
        # Normal(1, 215) gives:  q99 = 500
        () -> Truncated(Normal(1.0, 215.0), 1.0, 1000.0)
        # () -> Uniform(1.0, 1000.0)
        # () -> FixedValDist{Float}(10.0)
    ),

    adeAgeAvg = Stochastic(
        # Gamma(shape=5.03, scale=46.97) gives:
        #   q01 = 61 (2m);  q99 = 547 (18m);  mode = 189 (6.3m)
        () -> Truncated(Gamma(5.03, 46.97), 30.0, 730.0)
        # () -> Uniform(30.0, 730.0)
    ),

    adeAgeVar = Stochastic(
		# () -> Uniform(950, 1e4)
		# With adeAgeVar = 950: 95% of the ADE effect would occurs within
		#                       4 month around the mean.
		# With adeAgeVar = 8500: 95% of the ADE effect would occurs within
		#                        12 month around the mean.
		# Normal(950, 3230) gives:  q99 = 8504
		() -> truncated(Normal(950.0, 3230.0), 950, 17000.0)
		# (adeAgeStdev) -> adeAgeStdev * adeAgeStdev
    ),

    adeAgeStdev = Logical(
        (adeAgeVar) -> sqrt(adeAgeVar)
		# Normal(30.0, 38.7) gives:  q99 = 120
		# () -> truncated(Normal(30.0, 38.7), 30.0, 150.0)
    ),

    maternalProtectDuration_scale = Stochastic(
        () -> Uniform(0.0, 1.0)
    ),

    maternalProtectDuration = Logical(
        (maternalProtectDuration_scale, adeAgeAvg) ->
            adeAgeAvg * maternalProtectDuration_scale
    ),

	priSymptomaticProb = Stochastic(
        # Beta(α=1.35, β=7.59) gives:  q01 = 0.005;  q50 = 0.13;  q99 = 0.50
        # This is based on the paper of Hannah Clapham (2017)
		# () -> Beta(1.35, 7.59)
		() -> Uniform(0.0, 1.0)
    ),

	sndSymptomaticProb = Stochastic(
        # Beta(α=1.5, β=2.0) gives:  q01 = 0.03;  q50 = 0.41;  q99 = 0.93
        # This is based on the paper of Hannah Clapham (2017)
		() -> Beta(1.5, 2.0)
		# () -> Uniform(0.0, 1.0)
    ),

    crossProtectDuration = Stochastic(
        # Gamma(shape=2.66, scale=93.33) gives:
        #   q01 = 30 (1m);  q99 = 730 (24m);  mode = 155 (5.2m)
        # () -> Truncated(Gamma(2.66, 93.33), 20.0, 1095.0)

		# If crossProtectDuration is too low (<15 days), individuals would tend
		# to have 2 infections within the same year as a primary infection would
		# fuel up the chance for a secondary infection and vice versa.
		# If crossProtectDuration is too high (>182 days), the protection would
		# create a long period of very low susceptible population.
		() -> Uniform(30.0, 2000.0)
    ),

	crossProtectStrength = Stochastic(
		() -> Uniform(0.0, 1.0)
	),

	cprInfectionRisk = Logical(
		(crossProtectStrength) -> 1.0 - crossProtectStrength.value
	),

	serotypeRelProportions = Stochastic(
		1,
		(modelVars) -> UnivariateDistribution[
			Uniform(0.0, 1.0) for i = 1 : (modelVars.nSerotypes - 2)
		]
	),

	serotypeProportions = Logical(
		1,
		(sndSerotypeProp, serotypeRelProportions, modelVars) -> begin
			result = ones(Float, modelVars.nSerotypes)
			result[2] = sndSerotypeProp
			for serotype = 3 : modelVars.nSerotypes
				result[serotype] =
				 	result[serotype - 1] * serotypeRelProportions[serotype - 2]
			end
			return result
		end
	),

	simCaseCountsAndSeroprev = Logical(
		2,
		(primaryTranRate,      seasonalTranScales,
		 recoveryRate,         crossProtectDuration,
		 crossProtectStrength, hospRates,
		 adeEffectScale,       adeAgeAvg,
		 adeAgeVar,            maternalProtectDuration,
		 sndSymptomaticProb,   priSymptomaticProb,
		 sndInfectivity,       serotypeProportions,
		 modelVars,            odeInitState) -> begin

		 	# Create new a ModelParams object based on the sampled values
		 	serotypeTranRates =
			 	[primaryTranRate for i = 1 : modelVars.nSerotypes]
			newParams = ModelParams(
				serotypeTranRates,    seasonalTranScales,
				recoveryRate,         crossProtectDuration,
				crossProtectStrength, hospRates,
				adeEffectScale,       adeAgeAvg,
				adeAgeVar,            maternalProtectDuration,
				sndSymptomaticProb,   priSymptomaticProb,
				sndInfectivity
			)

			# Redistribute the serotypes in the initial ODE state
			DengueSIR.redistribute_serotypes!(
			    odeInitState, modelVars, serotypeProportions.value
			)

			# The ODEs is solved in this step
			odeSolution =
			 	DengueSIR.solve_odes(modelVars, newParams, odeInitState)

			# odeStates = serialise_ode_sol(odeSolution,
			#  								 modelVars.nDaysPerSummaryPeriod)

			priIncidences, sndIncidences =
				DengueSIR.compute_incidences(
					odeSolution,
					modelVars,
					newParams,
					modelVars.nDaysPerSummaryPeriod
				)

			# simCaseCounts is a matrix of size nAgeGroups × nPeriods
			simCaseCounts = DengueSIR.calculate_hospitalisations(
				modelVars, newParams, priIncidences, sndIncidences
			)

			seroprev = DengueSIR.compute_seroprevalence(
				odeSolution, modelVars
			)

			# This is a hack to avoid solving the ODEs multiple times
			return hcat(simCaseCounts, seroprev)
		end,
		false
	),

	simSeroprevalence = Logical(
		1,
		# The simulated seroprevalence is stored at the last column of of the
		# simCaseCountsAndSeroprev matrix
		(simCaseCountsAndSeroprev) -> simCaseCountsAndSeroprev[:, end]
	),

	seroprevalence = Stochastic(
		1,
		(simSeroprevalence, seroprevFitPrecision, modelVars) -> begin
			nAgeGroups = length(modelVars.ageGroupSizes)
			dists = Vector{UnivariateDistribution}(undef, nAgeGroups)
			for ageGroup = 1 : nAgeGroups
				expectSeroprev = simSeroprevalence[ageGroup]
				if !isnan(expectSeroprev)
					alpha = expectSeroprev * seroprevFitPrecision
					beta  = (1.0 - expectSeroprev) * seroprevFitPrecision
					dists[ageGroup] = Beta(alpha, beta)
				else
					dists[ageGroup] = FixedValDist{Float}(0.5)
				end
			end
			return dists
		end,
		false
	),

    expectCaseCounts = Logical(
		3,
        (nHospitals, nPeriods, hospWeights, modelVars,
         simCaseCountsAndSeroprev) -> begin
            simCaseCounts = simCaseCountsAndSeroprev[:, 1 : (end-1)]
			nAgeGroups = length(modelVars.ageGroupSizes)
            results = Array{Float, 3}(undef, nHospitals, nAgeGroups, nPeriods)
            for hospIdx = 1 : nHospitals, ageGroup = 1 : nAgeGroups
                weight = hospWeights[ageGroup, hospIdx]
                results[hospIdx, ageGroup, :] =
                    simCaseCounts[ageGroup, :] .* weight
            end
            return results
        end
    )

)

using DengueSIR
using Dates
using Mamba


function plot_chains(chains, fileName::String)
    chainPlot = Mamba.plot(chains, legend = true)
    nCols, nRows = size(chainPlot)
    draw(chainPlot, nrow = nRows, ncol = nCols,
         width = 20cm, height = 8cm * nRows, filename = fileName)
end


function filter_chain_data(chains)
    meaningfulParams = copy(chains.names)
    unplottedParams = [
        "expectCaseCounts", "simTotalCaseCounts",
        "serotypeTranRate_1", "seasonalTranScales_raw",
        "maternalProtectDuration_scale",
        "hospRate_", "simSeroprevalence"
    ]
    filter!((x) -> !any(startswith.(x, unplottedParams)), meaningfulParams)
    return chains[:, meaningfulParams, :]
end


function save_chains(chains, saveDir::String, fileNameSuffix::String = "")
    outFilePrefix = Dates.format(Dates.now(), "yymmdd_HHMMSS")
    mkpath(saveDir)
    write(saveDir * "/" * outFilePrefix * "_mcmc_chains" * fileNameSuffix *
          ".cdt", chains)

    plotData = filter_chain_data(chains)
    plot_chains(plotData, saveDir * "/" * outFilePrefix * "_mcmc_plots" *
                fileNameSuffix * ".svg")

    println("""
            Chain data saved:
              - File name prefix:  $outFilePrefix
              - File name suffix:  $fileNameSuffix
            """)
end



function get_infant_age_groups(modelVars::ModelVars) :: Vector{Int}
    nAgeGroups = length(modelVars.ageGroupSizes)
    maxAgeOfGroups = cumsum(modelVars.ageGroupSizes) / modelVars.nDaysPerYear
    minAgeOfGroups = vcat(0.0, maxAgeOfGroups[1 : end-1])
    allAgeGroupIdxs = [i for i = 1 : nAgeGroups]
    infantAgeGroupIdxs = filter(
        (i) -> 0.0 <= minAgeOfGroups[i] && maxAgeOfGroups[i] <= 2.0,
        allAgeGroupIdxs
    )
    return infantAgeGroupIdxs
end



function get_noninfant_age_groups(modelVars::ModelVars) :: Vector{Int}
    nAgeGroups = length(modelVars.ageGroupSizes)
    maxAgeOfGroups = cumsum(modelVars.ageGroupSizes) / modelVars.nDaysPerYear
    minAgeOfGroups = vcat(0.0, maxAgeOfGroups[1 : end-1])
    allAgeGroupIdxs = [i for i = 1 : nAgeGroups]
    noninfAgeGroupIdxs = filter(
        (i) -> 2.0 <= minAgeOfGroups[i] &&
               maxAgeOfGroups[i] <= (modelVars.maxAcceptAgeInDays /
                                     modelVars.nDaysPerYear),
        allAgeGroupIdxs
    )
    return noninfAgeGroupIdxs
end



function impute_missing_cases(caseCountMatrix::Array{Float, 3},
                              hospWeights::Matrix{Float};
                              ageGroups::Vector{Int} = Int[]) :: Array{Float, 3}
    nHospitals, nAgeGroups, nTimePoints = size(caseCountMatrix)
    if length(ageGroups) <= 0
        ageGroups = [i for i = 1 : nAgeGroups]
    end

    result = copy(caseCountMatrix)
    if nHospitals <= 1
        return result
    end

    for hosp = 1 : nHospitals
        for ageGroup in ageGroups
            currentWeight = hospWeights[ageGroup, hosp]
            for timePoint = 1 : nTimePoints
                if ( isnan(result[hosp, ageGroup, timePoint]) ||
                     result[hosp, ageGroup, timePoint] < 0 )
                    for anotherHosp = 1 : nHospitals
                        if ( anotherHosp != hosp &&
                             !isnan(result[anotherHosp, ageGroup, timePoint]) &&
                             result[anotherHosp, ageGroup, timePoint] >= 0 )
                            result[hosp, ageGroup, timePoint] =
                                result[anotherHosp, ageGroup, timePoint] /
                                hospWeights[ageGroup, anotherHosp] *
                                currentWeight
                        end
                    end
                end
            end
        end
    end

    return result
end




# savedChains = read("results/mcmc_chains", ModelChains)


# Int(0x44)
# dark_blue = "#0b486b", (11, 72, 107)
# teal      = "#3cafa0",
# grey      = "#777777",
# orange    = "#fd9644", (253, 150, 68)
# dark_red  = "#be2b4e"  (190, 43, 78)

using Distributed
@everywhere using DengueSIR
@everywhere using Mamba

include("model_partialCpr.jl")
include("../util.jl")


modelConfigFile = "mcmc/PartialCpr/modelConfig_allAges_partialCpr.yml"
dataFile        = "data/ch1_ch2_data.csv"
outputDir       = "results"
chainLength = 8000
burnin      = 3000
thinning    = 1
nChains     = 4

outputFileSuffix = "_partialCpr_fitSd20"


modelVars, initParams = DengueSIR.load_model_config(modelConfigFile)
hospData = DengueSIR.load_data_from_csv(dataFile, modelVars)

caseCountMatrix = DengueSIR.as_3d_array(hospData)
caseCountMatrix = impute_missing_cases(
    caseCountMatrix,
    hospData.hospitalWeights,
    ageGroups = get_noninfant_age_groups(modelVars)
)

odeInitState =  DengueSIR.init_sys_state(
    modelVars, initParams, nCalibratingYears = 97.0)

seroprevalence = DengueSIR.calculate_seroprev_from_foi(modelVars)


modelInputs = Dict{Symbol, Any}(
    :seroprevalence       => seroprevalence,
    :caseCountMatrix      => caseCountMatrix,
    :nHospitals           => hospData.nHospitals,
    :nPeriods             => hospData.nPeriods,
    :modelVars            => modelVars,
    :odeInitState         => odeInitState,
    :recoveryRate         => initParams.recoveryRate,
    :crossProtectStrength => initParams.crossProtectStrength,
    :hospWeights          => hospData.hospitalWeights,
    :sndInfectivity       => 1.0,
    :sndSerotypeProp      => 0.9,
    :fittingStdev         => 20.0
    # The MCMC will be stuck if fittingStdev falls too low.
    # fittingStdev = 18 seems to be optimal
)

maternalProtectDuration_scale =
    initParams.maternalProtectDuration / initParams.adeAgeAvg
maternalProtectDuration_scale = maternalProtectDuration_scale > 0.0 ?
                                maternalProtectDuration_scale : 0.5
primaryTranRate = sum(initParams.serotypeTranRates) / modelVars.nSerotypes
modelInitVals = [ Dict{Symbol, Any}(
    :seroprevalence          => seroprevalence,
    :seroprevFitPrecision    => 700.0,
    :caseCountMatrix         => caseCountMatrix,
    :crossProtectDuration    => initParams.crossProtectDuration,
    :primaryTranRate         => primaryTranRate,
    :seasonalTranScales_raw  => initParams.seasonalTranScales[2:end],
    :priSymptomaticProb      => initParams.priSymptomaticProb,
    :sndSymptomaticProb      => initParams.sndSymptomaticProb,
    :hospRate_2              => initParams.hospRates[2],
    :hospRate_diff           => 0.1,
    :adeEffectScale          => initParams.adeEffectScale,
    :adeAgeAvg               => initParams.adeAgeAvg,
    :adeAgeVar               => initParams.adeAgeVar,
    :serotypeRelProportions  => [0.9, 0.9],
    :maternalProtectDuration_scale => maternalProtectDuration_scale
) for i = 1 : nChains ]


## Sampling Scheme Assignment
adaptMode = :burnin
samplingScheme = [
    AMWG( [:serotypeRelProportions,
           # :crossProtectDuration,
           :primaryTranRate,
           :seroprevFitPrecision,
           :seasonalTranScales_raw],
          [10e-2, 9e-2,  # serotypeRelProportions
           # 0.6,          # crossProtectDuration
           10e-3,        # primaryTranRate
           0.8,          # seroprevFitPrecision
           4e-2, 3e-2],  # seasonalTranScales_raw
          adapt = adaptMode ),
    AMWG( [:hospRate_2, :hospRate_diff,
           :priSymptomaticProb,
           # :sndSymptomaticProb,
           :maternalProtectDuration_scale,
           :adeEffectScale,
           :adeAgeAvg,
           :adeAgeVar],
          [0.15, 1.0, # hospRate_2 & hospRate_diff
           0.1,       # priSymptomaticProb
           # 0.1,       # sndSymptomaticProb
           2.0,       # maternalProtectDuration_scale
           1.5,       # adeEffectScale
           1.5,       # adeAgeAvg
           3.0],      # adeAgeVar
          adapt = adaptMode )
]
setsamplers!(caseModel, samplingScheme)


using Dates
startTime = Dates.format(Dates.now(), "yyyy-mm-dd HH:MM:SS")
println("")
println("-----------------------------------------")
println("Analysis start time:  ", startTime)
println("-----------------------------------------")

chains = mcmc(caseModel, modelInputs, modelInitVals, chainLength,
              burnin = burnin, thin = thinning, chains = nChains)

endTime = Dates.format(Dates.now(), "yyyy-mm-dd HH:MM:SS")
println("-----------------------------------------")
println("Analysis end time  :  ", endTime)
println("-----------------------------------------")
println("")

save_chains(chains, outputDir, outputFileSuffix)

library(ggplot2)
library(RUtil)
library(DengueCases)

setPlotOpts( printPlotsOnSave = F,
             lockAfterChange = F,
             textSize = 11 )


plotLineWidth <- 0.8


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Create a function to calculate the effect of maternal immunity
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
maternalEffect <- function(ages,
                           maternalProtectDuration,
                           adeEffectScale,
                           adeAgeAvg,
                           adeAgeStdev,
                           priSymptomaticProb = 1,
                           hospRate_1 = 1,
                           hospRate_2 = 1) {
    results <- NULL
    for (age in ages) {
        if (age <= maternalProtectDuration) {
            results <- append(results, 0)
        } else {
            effect <- 1 +
                (adeEffectScale - 1) * exp(-(age - adeAgeAvg)^2 / (adeAgeStdev)^2)
            symptomaticProb <- effect * priSymptomaticProb
            if (age <= 730) {
                results <- append(results, symptomaticProb * hospRate_1)
            } else {
                results <- append(results, symptomaticProb * hospRate_2)
            }
        }
    }
    return(results)
}


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Load simulated yearly case counts
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# simAgeDist <- read.csv(
#     "data/200403_234705_mcmc_chains_fullCpr_fitSd20_simYearlyCaseCounts.csv",
#     stringsAsFactors = F
# )
#
# simAgeDist$hospital <- "Children's Hospital One"
# simAgeDist$hospital[which(simAgeDist$hospId == 2)] <- "Children's Hospital Two"
# simAgeDist$hospital <- as.factor(simAgeDist$hospital)
#
# simAgeDist$age <- simAgeDist$age - 0.5
#
# simAgeDist$admYearPeriod[which(simAgeDist$admYearPeriod == "2005:2006")] <- "[2005 : 2006]"
# simAgeDist$admYearPeriod[which(simAgeDist$admYearPeriod == "2007:2009")] <- "[2007 : 2009]"
# simAgeDist$admYearPeriod[which(simAgeDist$admYearPeriod == "2010:2012")] <- "[2010 : 2012]"
# simAgeDist$admYearPeriod[which(simAgeDist$admYearPeriod == "2013:2015")] <- "[2013 : 2015]"
# simAgeDist$admYearPeriod <- as.factor(simAgeDist$admYearPeriod)



## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Load and plot hospitalisation risk of each birth month
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# casePropOfBirthMonth <- read.csv(
#     "data/SIR/200403_234705_mcmc_chains_fullCpr_fitSd20_caseProportionsByMonthOfBirth.csv"
# )
hospRiskOfBirthMonth_SwComp <- read.csv(
    "data/SIR/200416_103243_mcmc_chains_fullCpr_fitSd20_hospRiskByMonthOfBirth.csv"
)
hospRiskOfBirthMonth_SwComp.df = NULL
for (i in 1 : ncol(hospRiskOfBirthMonth_SwComp)) {
    hospRiskOfBirthMonth_SwComp.df <- rbind(
        hospRiskOfBirthMonth_SwComp.df,
        data.frame(
            birthMonth = 1:12,
            hospRisk = hospRiskOfBirthMonth_SwComp[, i],
            sim = i,
            dataType = "Estimated Relative Risk of Hospitalisation\nDuring Infancy",
            model = "Model SwComp"
        )
    )
}


hospRiskOfBirthMonth_SyPart <- read.csv(
    "data/SIR/200419_152222_mcmc_chains_partialCpr_fitSd20_serotypesSynced_hospRiskByMonthOfBirth.csv"
)
hospRiskOfBirthMonth_SyPart.df = NULL
for (i in 1 : ncol(hospRiskOfBirthMonth_SyPart)) {
    hospRiskOfBirthMonth_SyPart.df <- rbind(
        hospRiskOfBirthMonth_SyPart.df,
        data.frame(
            birthMonth = 1:12,
            hospRisk = hospRiskOfBirthMonth_SyPart[, i],
            sim = i,
            dataType = "Estimated Relative Risk of Hospitalisation\nDuring Infancy",
            model = "Model SyPart"
        )
    )
}


# casePropOfBirthMonth.df = NULL
# for (i in 1 : ncol(casePropOfBirthMonth)) {
#     casePropOfBirthMonth.df <- rbind(
#         casePropOfBirthMonth.df,
#         data.frame(
#             birthMonth = 1:12, caseProp = casePropOfBirthMonth[, i], sim = i,
#             dataType = "Estimated Proportion of Infant Admissions\nContributed by Each Month of Birth"
#         )
#     )
# }



birthMonthPlot <- ggplot() +
    geom_line( data = hospRiskOfBirthMonth_SwComp.df,
               aes(x = birthMonth, y = hospRisk, group = sim),
               alpha = 0.1, col = "#BE2B4E" ) +
    geom_line( data = hospRiskOfBirthMonth_SyPart.df,
               aes(x = birthMonth, y = hospRisk, group = sim),
               alpha = 0.1, col = "#BE2B4E" ) +
    labs( x = "Calendar Month of Birth",
          y = "Relative Risk of Hospitalisation During Infancy" ) +
    scale_x_continuous( breaks = 1:12,
                        labels = c("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec") ) +
    coord_cartesian( ylim = c(0.045, 0.135) ) +
    facet_grid( ~model , scales = "free" )
print(birthMonthPlot)

pdf( getProjectRoot()
     %_% "reports/Relative Risk of Hospitalisation During Infancy.pdf",
     width = 9, height = 4, pointsize = 11 )
birthMonthPlot
dev.off()



## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Load estimated parameters and create a summary table
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
params <- read.csv("data/SIR/200416_103243_mcmc_chains_fullCpr_fitSd20_params.csv")
# params <- read.csv("data/SIR/200419_152222_mcmc_chains_partialCpr_fitSd20_serotypesSynced_params.csv")
# selectedRows <- runif(1000, min = 1, max = nrow(params))
# params <- params[selectedRows, ]

paramSummary <- data.frame(param = names(params))
paramSummary$median <- apply(params, 2, median)
paramSummary$ci025  <- apply(params, 2, quantile, probs = 0.025)
paramSummary$ci975  <- apply(params, 2, quantile, probs = 0.975)
# paramSummary$ci250  <- apply(params, 2, quantile, probs = 0.25)
# paramSummary$ci750  <- apply(params, 2, quantile, probs = 0.75)
for (col in 2 : 4) {
    paramSummary[, col] <- formatC(paramSummary[, col], digits = 4, format = "f")
}


# params_fullCpr <- paramSummary
# names(params_fullCpr)[2:4] <- paste0("fullCpr_", names(params_fullCpr)[2:4])
# params_partCpr <- paramSummary
# names(params_partCpr)[2:4] <- paste0("partCpr_", names(params_partCpr)[2:4])
# paramSummary <- merge(params_fullCpr, params_partCpr, all = T)


# write.csv(paramSummary, file = "reports/MCMC param summary.csv", row.names = F)


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Plot the effect of maternal immunity
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# params <- read.csv("data/SIR/200416_103243_mcmc_chains_fullCpr_fitSd20_params.csv")
# params <- read.csv("data/SIR/200419_152222_mcmc_chains_partialCpr_fitSd20_serotypesSynced_params.csv")

ages <- seq(0, 2 * 365, length.out = 500)
symptomaticRatesMatrix <- matrix(nrow = length(ages), ncol = nrow(params))
for (i in 1 : nrow(params)) {
    symptomaticRatesMatrix[, i] <- maternalEffect(
        ages,
        maternalProtectDuration = params$maternalProtectDuration[i],
        adeEffectScale          = params$adeEffectScale[i],
        adeAgeAvg               = params$adeAgeAvg[i],
        adeAgeStdev             = params$adeAgeStdev[i],
        # priSymptomaticProb      = 1
        priSymptomaticProb      = params$priSymptomaticProb[i]
        # hospRate_1              = params$hospRates_1[i],
        # hospRate_2              = params$hospRates_2[i]
    )
}

symptomaticRates <- data.frame(age = ages / 365)
symptomaticRates$mid <- apply(symptomaticRatesMatrix, 1, median)
symptomaticRates$ci025  <- apply(symptomaticRatesMatrix, 1, quantile, probs = 0.025)
symptomaticRates$ci250  <- apply(symptomaticRatesMatrix, 1, quantile, probs = 0.25)
symptomaticRates$ci750  <- apply(symptomaticRatesMatrix, 1, quantile, probs = 0.75)
symptomaticRates$ci975  <- apply(symptomaticRatesMatrix, 1, quantile, probs = 0.975)


plotPriSymptomRate <-
    ggplot( symptomaticRates, aes(x = age) ) +
    geom_ribbon( aes(ymin = ci025, ymax = ci975), fill = "#BE2B4E", alpha = 0.2 ) +
    geom_ribbon( aes(ymin = ci250, ymax = ci750), fill = "#BE2B4E", alpha = 0.3 ) +
    geom_line( aes(y = mid), col = "#BE2B4E", size = 1.0 ) +
    labs( x = "Age (months)",
          y = "Probability of Developing Symptoms During a Primary Infection of Maternally Immune Infants") +
    scale_x_continuous( breaks = seq(0, 3, by = 0.25), labels = seq(0, 36, by = 3) ) +
    scale_y_continuous( breaks = seq(0, 0.3, by = 0.05) ) +
    coord_cartesian( ylim = c(0, 0.25) )

# plotPriSymptomRate <-
#     ggplot( symptomaticRates, aes(x = age) ) +
#     geom_ribbon( aes(ymin = ci025, ymax = ci975), fill = "#BE2B4E", alpha = 0.2 ) +
#     geom_ribbon( aes(ymin = ci250, ymax = ci750), fill = "#BE2B4E", alpha = 0.3 ) +
#     geom_line( aes(y = mid), col = "#BE2B4E", size = 1.0 ) +
#     labs( x = "Age (months)",
#           y = "Probability of Developing Symptoms During a Primary Infection") +
#     scale_x_continuous( breaks = seq(0, 3, by = 0.25), labels = seq(0, 36, by = 3) ) +
#     scale_y_continuous( trans='sqrt', breaks = c(1, 10, 25, seq(50, 250, by = 50)) ) +
#     coord_cartesian( ylim = c(0, 250) )

print(plotPriSymptomRate)

# pdf( getProjectRoot() %_% "reports/TypeSynch-P.pdf",
# pdf( getProjectRoot() %_% "reports/TypeSwtch-C.pdf",
#      width = 5, height = 5, pointsize = 11 )
# plotPriSymptomRate
# dev.off()


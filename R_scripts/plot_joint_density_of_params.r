setwd("~/Workspace/DengueSIR/results/good_1.2")


library(ggplot2)
library(GGally)
library(hexbin)

library(RUtil)
library(DengueCases)

setPlotOpts( printPlotsOnSave = F,
             lockAfterChange = F,
             textSize = 11 )


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Load estimated values of parameters
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
dataName <- "200416_103243_mcmc_chains_fullCpr_fitSd20_params"
# dataName <- "200419_152222_mcmc_chains_partialCpr_fitSd20_serotypesSynced_params"

params <- read.csv(dataName %_% ".csv")


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Summary parameter values
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
paramSummary <- data.frame(param = names(params))
paramSummary$median <- apply(params, 2, median)
paramSummary$ci025  <- apply(params, 2, quantile, probs = 0.025)
paramSummary$ci975  <- apply(params, 2, quantile, probs = 0.975)

for (col in 2 : 4) {
    paramSummary[, col] <- formatC(paramSummary[, col], digits = 4, format = "f")
}



## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Generate pairwise joint posterior plots
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
plottedParamNames <- c(
    "hospRates_1", "hospRates_2",
    # "serotypeProportions_3", "serotypeProportions_4",
    "priSymptomaticProb", "crossProtectDuration",
    "adeEffectScale", "adeAgeStdev", "adeAgeAvg", "maternalProtectDuration",
    "seasonalTranRates_1", "seasonalTranRates_2", "seasonalTranRates_3"
)


plottedData <- params[, plottedParamNames]

## Preserve data points that are within the 95% credible intervals
for ( col in 1 : ncol(plottedData) ) {
    minVal <- as.numeric(
        paramSummary[paramSummary$param == names(plottedData)[col], "ci025"]
    )
    maxVal <- as.numeric(
        paramSummary[paramSummary$param == names(plottedData)[col], "ci975"]
    )

    colData <- plottedData[, col]
    colData[which(colData < minVal | colData > maxVal)] <- NA
    plottedData[, col] <- colData
}


## Function to create each plot panel
plotJointDensity <- function(data, mapping, ...) {
    ggplot(data = data, mapping = mapping, ...) +
        stat_density_2d( ..., aes(fill = ..density..), geom = "raster",
                         contour = FALSE ) +
        scale_x_continuous( expand = c(0, 0) ) +
        scale_y_continuous( expand = c(0, 0) ) +
        scale_fill_continuous(type = "viridis")
}



pairPlots <- ggpairs( data = plottedData,
                      lower = list(continuous = plotJointDensity) )
print(pairPlots)


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Save the plot to a PDF file
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{
    pdf( dataName %_% ".pdf", width = 8.5, height = 8.2, useDingbats = F )
    print(pairPlots)
    dev.off()
}


library(ggplot2)
library(RUtil)
library(DengueCases)
library(gridExtra)

setPlotOpts( printPlotsOnSave = F,
             lockAfterChange = F,
             textSize = 11 )


plotLineWidth <- 0.7


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Load data
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
caseCount <- read.csv(
    "../results/good_1.2/200416_103243_mcmc_chains_fullCpr_fitSd20_simCaseCountSummary.csv"
)

caseCount <- caseCount[which(caseCount$maxAge <= 24), ]
# caseCount <- caseCount[which(caseCount$maxAge <= (14 * 12)), ]

caseCount$realCaseCount[which(caseCount$realCaseCount <= 0)] <- NA
caseCount$simCaseCount_025[which(caseCount$simCaseCount_025 <= 0)] <- NA
caseCount$simCaseCount_500[which(caseCount$simCaseCount_500 <= 0)] <- NA
caseCount$simCaseCount_975[which(caseCount$simCaseCount_975 <= 0)] <- NA


TypeSwtchC <- ggplot( data = caseCount ) +
    geom_rect( aes(xmin = minAge, xmax = maxAge, ymax = realCaseCount, ymin = 0),
               col = "#ececec", fill = "#a0aab8", size = 0.3 ) +
    geom_linerange( aes(x = avgAge, ymin = simCaseCount_025,
                        ymax = simCaseCount_975),
                    size = plotLineWidth, col = "dark orange 3", alpha = 0.9 ) +
    geom_point( aes(x = avgAge, y = simCaseCount_500),
                size = 1.3, col = "dark orange 3" ) +
    labs( x = "Age (months)",
          y = "Average Number of Cases per Year per Age Group of 4 Weeks") +
    coord_cartesian( ylim = c(0, 50) ) +
    scale_x_continuous( breaks = seq(0, 24, 3), expand = c(0.015, 0.015) ) +
    facet_grid( hospital~yearPeriod, as.table = T, scales = "fixed" ) +
    theme( panel.background = element_rect(fill = "#ececec"),
           panel.grid = element_line(color = "#cccccc") )

# print(TypeSwtchC)


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
caseCount <- read.csv(
    "../results/good_1.2/200419_152222_mcmc_chains_partialCpr_fitSd20_serotypesSynced_simCaseCountSummary.csv"
)

caseCount <- caseCount[which(caseCount$maxAge <= 24), ]
# caseCount <- caseCount[which(caseCount$maxAge <= (14 * 12)), ]

caseCount$realCaseCount[which(caseCount$realCaseCount <= 0)] <- NA
caseCount$simCaseCount_025[which(caseCount$simCaseCount_025 <= 0)] <- NA
caseCount$simCaseCount_500[which(caseCount$simCaseCount_500 <= 0)] <- NA
caseCount$simCaseCount_975[which(caseCount$simCaseCount_975 <= 0)] <- NA

TypeSynchP <- ggplot( data = caseCount ) +
    geom_rect( aes(xmin = minAge, xmax = maxAge, ymax = realCaseCount, ymin = 0),
               col = "#ececec", fill = "#a0aab8", size = 0.3 ) +
    geom_linerange( aes(x = avgAge, ymin = simCaseCount_025,
                        ymax = simCaseCount_975),
                    size = plotLineWidth, col = "dark orange 3", alpha = 0.9 ) +
    geom_point( aes(x = avgAge, y = simCaseCount_500),
                size = 1.3, col = "dark orange 3" ) +
    labs( x = "Age (months)",
          y = "Average Number of Cases per Year per Age Group of 4 Weeks") +
    coord_cartesian( ylim = c(0, 50) ) +
    scale_x_continuous( breaks = seq(0, 24, 3), expand = c(0.015, 0.015) ) +
    facet_grid( hospital~yearPeriod, as.table = T, scales = "fixed" ) +
    theme( panel.background = element_rect(fill = "#ececec"),
           panel.grid = element_line(color = "#cccccc") )


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
pdf( "../results/good_1.2/Infant age distribution -- TypeSwtchC & TypeSynchP.pdf",
     width = 8.5, height = 9, useDingbats = F )
gridExtra::grid.arrange(TypeSwtchC, TypeSynchP)
dev.off()


using DengueSIR
import Plots

include("../mcmc/util.jl")
include("../mcmc/post_mcmc_util.jl")


colSums(matrix::Matrix) = sum(matrix, dims = 1)[1, :]


# modelConfigFile = "mcmc/allAges/modelConfig_allAges_simple.yml"
# modelConfigFile = "mcmc/allAges/modelConfig_allAges_semiCpr.yml"
modelConfigFile = "mcmc/FullCpr/modelConfig_allAges_fullCpr.yml"
modelVars, initParams = DengueSIR.load_model_config(modelConfigFile)

modelVars = ModelVars(
    modelVars.ageGroupSizes,
    modelVars.nDaysPerYear,
    modelVars.nYearsToFit,
    modelVars.nDaysReportingLag,
    modelVars.minAcceptAgeInDays,
    modelVars.maxAcceptAgeInDays,
    modelVars.nDaysPerSummaryPeriod,
    modelVars.maxConsecutiveZeroDays,
    modelVars.movingAvgWindowSize,
    modelVars.movingAvgWeight,
    modelVars.initPopSize,
    modelVars.annualNatPopGrowthRates,
    modelVars.annualNetMigRates,
    # [1/12 for i = 1:12],  # modelVars.seasonalBirthRates,
    1 ./ modelVars.seasonalBirthRates,
    modelVars.nSerotypes,
    modelVars.nSeasonalTranPeriods,
    modelVars.hospRateCutoffs,
    modelVars.startYearOfAvailFOI,
    modelVars.nYearsOfAvailFOI,
    modelVars.availFOI,
    modelVars.nCrossProtectSubComps,
    modelVars.nOdeWarmupYears,
    modelVars.odeAbsTolerance,
    modelVars.odeRelTolerance
)


dataFile = "data/ch1_ch2_data.csv"
hospData = DengueSIR.load_data_from_csv(dataFile, modelVars)

caseCountMatrix = DengueSIR.as_3d_array(hospData)
caseCountMatrix = impute_missing_cases(
    caseCountMatrix,
    hospData.hospitalWeights,
    ageGroups = get_noninfant_age_groups(modelVars)
)


# initParams = ModelParams(
#     [0.4 for i = 1:4],
#     [1.00, 1.12, 1.15, 1.10],
#     1/7,    # recoveryRate
#     200.0 , # crossProtectDuration
#     1.0,    # crossProtectStrength
#     [0.25, 0.1],  # hospRates
#     10.0,    # adeEffectScale
#     240.5,   # adeAgeAvg
#     3000.0,  # adeAgeVar
#     0.0,     # maternalProtectDuration
#     0.41,    # sndSymptomaticProb
#     0.13,    # priSymptomaticProb
#     1.0      # sndInfectivity
# )

odeInitState =  DengueSIR.init_sys_state(
    modelVars, initParams, nCalibratingYears = 97.0
)

DengueSIR.redistribute_serotypes!(
    odeInitState, modelVars, [1.0, 0.9, 0.71, 0.66]
)


let hospIdx = 1, ageGroup =  1:20, primTranRate = 0.39,
    odeClassIndexer = SIRColumnIndexer(modelVars),
    newParams = ModelParams(
        [primTranRate for i = 1:4],
        # [0.4, 0.385, 0.38, 0.37],
        [1.00, 1.19, 1.11],
        1/7,    # recoveryRate
        1000.0,  # crossProtectDuration
        0.0,    # crossProtectStrength
        [0.16, 0.14],  # hospRates
        1.0,  # adeEffectScale
        243.63, # adeAgeAvg
        3463.1, # adeAgeVar
        104.68, # maternalProtectDuration
        0.41,   # sndSymptomaticProb
        0.13,   # priSymptomaticProb
        1.0     # sndInfectivity
    )

    realPlotData = map( (x) -> x < 0 ? NaN : x,
                        caseCountMatrix[hospIdx, ageGroup, :] )
    realPlotData = colSums(realPlotData)
    times = [
        (periodIdx - 1) * modelVars.nDaysPerSummaryPeriod / modelVars.nDaysPerYear
        for periodIdx = 1 : length(realPlotData)
    ] .+ 2005
    maxAgeOfGroups = cumsum(modelVars.ageGroupSizes) / modelVars.nDaysPerYear
    minAgeOfGroups = vcat(0.0, maxAgeOfGroups[1 : end-1])
    maxAge = round(maximum(maxAgeOfGroups[ageGroup]), digits = 2)
    minAge = round(minimum(minAgeOfGroups[ageGroup]), digits = 2)

    odeSolution = DengueSIR.solve_odes(modelVars, newParams, odeInitState)


    priSusceptbData = extract_ode_class(
        odeSolution,
        odeClassIndexer.priSusceptbIdx,
        modelVars.nDaysPerSummaryPeriod
    )
    priSusceptbPlot = Plots.plot(times, colSums(priSusceptbData[ageGroup, :]),
                                 xticks = [i for i = 2005 : 2016],
                                 title = "Primary Susceptible")
    priRecoverdPlot = Plots.plot(xticks = [i for i = 2005 : 2016],
                                 title = "Primary Recovered")
    sndSusceptbPlot = Plots.plot(xticks = [i for i = 2005 : 2016],
                                 title = "Secondary Susceptible")
    priInfectedPlot = Plots.plot(xticks = [i for i = 2005 : 2016],
                                 title = "Primary Infected")
    sndInfectedPlot = Plots.plot(xticks = [i for i = 2005 : 2016],
                                 title = "Secondary Infected")
    for serotype = 1 : modelVars.nSerotypes
        priRecoverdData = extract_ode_class(
            odeSolution,
            odeClassIndexer.priRecovredIdxs[serotype, 1],
            modelVars.nDaysPerSummaryPeriod
        )
        sndSusceptbData = extract_ode_class(
            odeSolution,
            odeClassIndexer.sndSusceptbIdxs[serotype],
            modelVars.nDaysPerSummaryPeriod
        )
        priInfectData = extract_ode_class(
            odeSolution,
            odeClassIndexer.priInfectedIdxs[serotype],
            modelVars.nDaysPerSummaryPeriod
        )
        sndInfectData = extract_ode_class(
            odeSolution,
            odeClassIndexer.sndInfectedIdxs[serotype],
            modelVars.nDaysPerSummaryPeriod
        )
        Plots.plot!(priRecoverdPlot, times,
                    colSums(priRecoverdData[ageGroup, :]))
        Plots.plot!(sndSusceptbPlot, times,
                    colSums(sndSusceptbData[ageGroup, :]))
        Plots.plot!(priInfectedPlot, times,
                    colSums(priInfectData[ageGroup, :]))
        Plots.plot!(sndInfectedPlot, times,
                    colSums(sndInfectData[ageGroup, :]))
    end
    infectedPlot = Plots.plot(
        priRecoverdPlot, sndSusceptbPlot, priInfectedPlot, sndInfectedPlot,
        layout = (2, 2), size = (1200, 800), legend = false
    )
    display(infectedPlot)
    Plots.savefig(infectedPlot, resultDir * "/ode.pdf")
    # display(priSusceptbPlot)
    # display(infectedPlot)


    # serialOdeStates =
    #     DengueSIR.serialise_ode_sol(odeSolution, modelVars.nDaysPerSummaryPeriod)
    # DengueSIR.deserialise_ode_sol(serialOdeStates, modelVars)

    priIncidences, sndIncidences =
        DengueSIR.compute_incidences(
            odeSolution,
            modelVars,
            newParams,
            modelVars.nDaysPerSummaryPeriod
        )

    simCaseCounts = DengueSIR.calculate_hospitalisations(
        modelVars, newParams, priIncidences, sndIncidences
    )

    @info sum(simCaseCounts[ageGroup, :]) / 11.0

    weight = hospData.hospitalWeights[ageGroup, hospIdx]
    simPlotData = simCaseCounts[ageGroup, :] .* weight
    simPlotData = colSums(simPlotData)
    # println(minimum(simPlotData), "\t", maximum(simPlotData))

    casePlot = Plots.plot(times, realPlotData,
                   title = "Children's Hospital $hospIdx | Age group $minAge - $maxAge",
                   lw = 1)

    Plots.plot!(casePlot, times, simPlotData, xticks = [i for i = 2005 : 2016],
                ylim = (0, 40),
                lw = 3, legend = false)
    display(casePlot)


    # seroprev = DengueSIR.compute_seroprevalence(odeSolution, modelVars)
    # seroprev_real = DengueSIR.calculate_seroprev_from_foi(modelVars)
    # avgAgesInYear = (maxAgeOfGroups + minAgeOfGroups) / 2.0
    # seroprevPlot = Plots.plot(avgAgesInYear, seroprev_real)
    # Plots.plot!(seroprevPlot, avgAgesInYear, seroprev, lw = 3, legend = false)
    # display(seroprevPlot)

    # Plots.savefig(
    #     casePlot,
    #     "results/Simulated cases for CH" * string(hospIdx) *
    #         " (avgTranRate=" * string(primTranRate) * ").pdf"
    # )
end

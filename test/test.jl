# using Revise
using DengueSIR
using Mamba
import Plots
# import Gadfly

include("../mcmc/util.jl")


dataFile        = "data/ch1_ch2_data.csv"
# modelConfigFile = "data/model_config.yml"
modelConfigFile = "data/model_config_all_ages.yml"
modelVars, initParams = DengueSIR.load_model_config(modelConfigFile)
hospData = DengueSIR.load_data_from_csv(dataFile, modelVars)
caseCountMatrix = DengueSIR.as_3d_array(hospData)


# import CSV
# using DataFrames
# CSV.write("test/data.csv", DataFrame(hospData.caseCounts[2]))


# include("model_for_over2y_only.jl")

chains = read("results/191227_131636_mcmc_chains_all_ages_2tranRates.cdt",
              ModelChains)
# chains = filter_chain_data(chains)
changerate(chains)
# describe(chains)
# keys(chains.model, :stochastic)
# chains.model

lpchains = logpdf(chains)
# lpchains = logpdf(chains, :caseCountMatrix)
lpVals = lpchains.value[:, 1, 1]
Plots.plot( lpVals )
minimum(lpVals)
maximum(lpVals)
changerate(lpchains)


simCaseMatrix, simSizes = load_sim_expect_case_count(chains)

let p = Plots.plot(), nSimLines = 100, nBurnin = 100, hospIdx = 1,  ageGroup = 10
    realPlotData = map( (x) -> x < 0 ? NaN : x,
                        caseCountMatrix[hospIdx, ageGroup, :] )
    Plots.plot!(p, realPlotData, lw = 2,
                title = "Hospital $hospIdx | Age group $ageGroup")

    nSims = simSizes[:nSims]
    nKepts = nSims - nBurnin
    stepSize = max(div(nKepts, nSimLines), 1)
    nLines   = max(div(nKepts, stepSize), 1)
    opacity  = min(5 / nLines, 1.0)
    for simIdx = (nBurnin + 1) : stepSize : nSims
        Plots.plot!(p, simCaseMatrix[simIdx, hospIdx, ageGroup, :],
                    lw = 2, legend = false,
                    linecolor = Plots.RGBA(0.9, 0.1, 0.1, opacity) )
    end
    display(p)
end

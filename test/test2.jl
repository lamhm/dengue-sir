# include("../src/globaldef.jl")
#
# include("../src/Util.jl")
# include("../src/ModelConfigs.jl")
# include("../src/HospitalData.jl")
# include("../src/DengueOdes.jl")
# include("../src/DengueModels.jl")
#
# using .ModelConfigs
# using .HospitalData
# using .DengueOdes
# using .DengueModels

using DengueSIR
using Mamba
import Plots

modelConfigFile = "mcmc/FullCpr/modelConfig_allAges_fullCpr.yml"
dataFile        = "data/ch1_ch2_data.csv"
modelVars, modelParams = load_model_config(modelConfigFile)
hospData = load_data_from_csv(dataFile, modelVars)


@show round.(calculate_seroprev_from_foi(modelVars), digits = 4)


odeInitState = DengueSIR.init_sys_state(modelVars, modelParams)
odeSolution  = DengueSIR.solve_odes(modelVars, newParams, odeInitState)
pri_inc, snd_inc = DengueSIR.compute_incidences(
    odeSolution,
    modelVars,
    modelParams,
    1
)

seroprev = DengueSIR.compute_seroprevalence(odeSolution, modelVars)
seroprev_real = DengueSIR.calculate_seroprev_from_foi(modelVars)
# seroprev

maxAges = cumsum(modelVars.ageGroupSizes)
avgAgesInYear =
    (maxAges - (modelVars.ageGroupSizes ./ 2.0)) / modelVars.nDaysPerYear

Plots.plot(avgAgesInYear, seroprev_real)
Plots.plot!(avgAgesInYear, seroprev)

Plots.plot(sum(pri_inc, dims = 1)[1,:])

nGroupsPerYear = 6
ageGroupSizes = [365 / nGroupsPerYear for i = 1 : (3 * nGroupsPerYear)]
maternalEffect = maternal_effects(ageGroupSizes, 90.0, 240.0, 4000.0, 3.0)
Plots.plot(cumsum(ageGroupSizes), maternalEffect)

expectSeroprev = 0.5
seroprevFitPrecision = 100
alpha = expectSeroprev * seroprevFitPrecision
beta  = (1.0 - expectSeroprev) * seroprevFitPrecision
print(logpdf(Beta(alpha, beta), expectSeroprev))
print(logpdf(Beta(alpha, beta), expectSeroprev + 0.05))
print(logpdf(Beta(alpha, beta), expectSeroprev - 0.05))


# totalCaseCounts = chains.value[1, 20:162, 1]
# Plots.plot(totalCaseCounts)



# Post-MCMC analysis
# describe(chains)
# gewekediag(chains)



#-------------------------------------------------------------------------------
import CSV
CSV.write("results/191013 MCMC run.csv", DataFrame(mcmc.value.data[:, :, 1]),
          header = mcmc.value.axes[2].val)
#-------------------------------------------------------------------------------

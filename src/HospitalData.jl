module HospitalData

include("globaldef.jl")

using ..ModelConfigs
using ..Util

import CSV
import DataFrames
import Dates: Date, Day



struct HospData
    caseCounts::Vector{Matrix{Float}}

    #= The proportion of cases contributed by each hospital.
       Each column corresponds to a hospital; each row corresponds to an age
       group. The sum of each row must be 1.0. =#
    hospitalWeights::Matrix{Float}

    #= Number of hospitals. =#
    nHospitals::Int

    #= Number of age groups. =#
    nAgeGroups::Int

    #= Number of summary time points. =#
    nPeriods::Int

    #= Contructor method. =#
    function HospData(nHospitals::Int, nAgeGroups::Int, nPeriods::Int)
        new(Vector{Matrix{Float}}(undef, nHospitals),
            [(1.0 / nHospitals) for row = 1 : nAgeGroups, timePoint = 1 : nHospitals],
            nHospitals,
            nAgeGroups,
            nPeriods)
    end
end  # struct HospData
export HospData


function update_hospital_weights!(
            hospData::HospData,
            ageGroupBlocks::Vector{Vector{Int}} = Vector{Vector{Int}}(undef, 0))

    if hospData.nHospitals <= 1
        return
    end

    if length(ageGroupBlocks) <= 0
        ageGroupBlocks = Vector{Vector{Int}}(undef, 1)
        ageGroupBlocks[1] = [i for i = 1 : hospData.nAgeGroups]
    end

    for ageGroupBlock in ageGroupBlocks
        if length(ageGroupBlock) <= 0
            continue
        end

        totalCasesOfEachHosp = zeros(Float, hospData.nHospitals)

        for timePoint = 1 : hospData.nPeriods
            sumCasesAtTimePoint = zeros(Float, hospData.nHospitals)

            isDataMissing = false
            for hospIdx in 1 : hospData.nHospitals, ageGroup in ageGroupBlock
                nCases = hospData.caseCounts[hospIdx][ageGroup, timePoint]
                if nCases >= 0
                    sumCasesAtTimePoint[hospIdx] += nCases
                else
                    isDataMissing = true
                    break
                end
            end

            if !isDataMissing
                totalCasesOfEachHosp += sumCasesAtTimePoint
            end
        end

        totalCases = sum(totalCasesOfEachHosp)
        if totalCases > 0
            hospData.hospitalWeights[ageGroupBlock, :] .=
                transpose(totalCasesOfEachHosp ./ totalCases)
        end
    end
end  # function update_hospital_weights!


"""
Load case data from a CSV file into a `HospData` object.
"""
function load_data_from_csv(csvFilePath::String,
                            modelVars::ModelVars;
                            colNameHospital::String  = "hospitalCode",
                            colNameAdmYear::String   = "admYear",
                            colNameAdmMonth::String  = "admMonth",
                            colNameAdmDay::String    = "admDay",
                            colNameAgeInDays::String = "ageInDays"
                            ) :: HospData

    ageGroupSizes          = modelVars.ageGroupSizes
    minAcceptAgeInDays     = modelVars.minAcceptAgeInDays
    maxAcceptAgeInDays     = modelVars.maxAcceptAgeInDays
    nYearsToLoad           = modelVars.nYearsToFit
    nDaysPerYear           = modelVars.nDaysPerYear
    nDaysPerSummaryPeriod  = modelVars.nDaysPerSummaryPeriod
    maxConsecutiveZeroDays = modelVars.maxConsecutiveZeroDays

    nDaysToLoad = Int(round(nYearsToLoad * nDaysPerYear))
    nPeriodsToLoad = div(nDaysToLoad, nDaysPerSummaryPeriod)

    if isnan(minAcceptAgeInDays)
        minAcceptAgeInDays = 0
    end
    if isnan(maxAcceptAgeInDays)
        maxAcceptAgeInDays = sum(ageGroupSizes)
    end

    csvDataFrame = CSV.read(csvFilePath, DataFrames.DataFrame, header=1)
    hospitalDict = Dict{String, Int}()
    hospitalList = unique(csvDataFrame[:, Symbol(colNameHospital)])
    nHospitals = length(hospitalList)
    for hospIndex in 1 : nHospitals
        hospitalName = hospitalList[hospIndex]
        hospitalDict[hospitalName] = hospIndex
    end

    nAgeGroups = length(ageGroupSizes)
    ageBoundaries = Vector{Float}(undef, nAgeGroups)
    ageBoundaries[1] = ageGroupSizes[1]
    if nAgeGroups > 1
        for i in 2 : nAgeGroups
            ageBoundaries[i] = ageBoundaries[i - 1] + ageGroupSizes[i]
        end
    end

    #=
    Transform the case data frame in to a vector of matrices of case count per day
    per age group. Each matrix in this vector represents the data of a hospital.
    =#
    dailyCaseCounts = Vector{Matrix{Float}}(undef, nHospitals)
    for i in 1 : nHospitals
        dailyCaseCounts[i] = zeros(nAgeGroups, nDaysToLoad)
    end

    minAdmYear = minimum(csvDataFrame[:, Symbol(colNameAdmYear)])
    firstAdmDate = Date(minAdmYear, 1, 1)

    for csvRow in 1 : size(csvDataFrame, 1)
        hospitalName = csvDataFrame[csvRow, Symbol(colNameHospital)]
        hospIndex = hospitalDict[hospitalName]
        admDate = Date(
            csvDataFrame[csvRow, Symbol(colNameAdmYear)],
            csvDataFrame[csvRow, Symbol(colNameAdmMonth)],
            csvDataFrame[csvRow, Symbol(colNameAdmDay)]
        )
        admDateIndex = Day(admDate - firstAdmDate).value + 1
        if admDateIndex <= nDaysToLoad
            ageInDays = csvDataFrame[csvRow, Symbol(colNameAgeInDays)]
            if minAcceptAgeInDays <= ageInDays <= maxAcceptAgeInDays
                ageGroupIndex = 1
                while ageInDays > ageBoundaries[ageGroupIndex]
                    ageGroupIndex += 1
                end
                if ageGroupIndex <= nAgeGroups
                    dailyCaseCounts[hospIndex][ageGroupIndex, admDateIndex] += 1
                end
            end
        end
    end

    #=
    Create blocks of age groups. These will be used for detecting missing data
    points and for calculating the weight of each hospital.
    =#
    maxAgeOfGroups = cumsum(ageGroupSizes)
    ageGroupIdxs = [i for i = 1 : length(ageGroupSizes)]
    under2yGroups = filter((i) -> (maxAgeOfGroups[i] <= nDaysPerYear * 2),
                                  ageGroupIdxs)
    over2yGroups =
        filter((i) -> (nDaysPerYear * 2 < maxAgeOfGroups[i] <= maxAcceptAgeInDays),
               ageGroupIdxs)
    missingGroups = filter((i) -> !(i in under2yGroups || i in over2yGroups),
                           ageGroupIdxs)
    ageGroupBlocks = [under2yGroups, over2yGroups, missingGroups]

    #=
    Summarise case counts per day into case counts per summary period.
    availAgeGroups stores the indices of the age groups whose case counts exist
    =#
    hospData = HospData(nHospitals, nAgeGroups, nPeriodsToLoad)
    for hospIdx = 1 : nHospitals
        nCasesPerPeriod = Util.group_matrix_columns(
            dailyCaseCounts[hospIdx],
            nDaysPerSummaryPeriod,
            maxConsecutiveZeros = maxConsecutiveZeroDays,
            missingMask         = -1,
            rowBlocks           = ageGroupBlocks
        )
        hospData.caseCounts[hospIdx] = nCasesPerPeriod
    end

    #= Calculate the weight of each hospital =#
    update_hospital_weights!(hospData, ageGroupBlocks)

    #= Smooth the case count series if needed =#
    if modelVars.movingAvgWindowSize >= 2
        for hospIdx = 1 : nHospitals, ageGroup = 1 : length(ageGroupSizes)
            hospData.caseCounts[hospIdx][ageGroup, :] =
                Util.movingAvg( hospData.caseCounts[hospIdx][ageGroup, :],
                                modelVars.movingAvgWindowSize,
                                modelVars.movingAvgWeight,
                                validateFunc = ((x) -> x >= 0) )
        end
    end

    return hospData
end  # function
export(load_data_from_csv)



"""
Transform the case data set into a 3D array, of which the 1st dimension
corresponds to hospitals, the 2nd dimension corresponds to age groups, and the
3rd dimension corresponds to summary time points.
"""
function as_3d_array(hospData::HospData) :: Array{Float, 3}
    caseCountMatrix =
        Array{Float, 3}(undef,               hospData.nHospitals,
                        hospData.nAgeGroups, hospData.nPeriods)
    for hospIdx in 1 : hospData.nHospitals
        caseCountMatrix[hospIdx, :, :] = hospData.caseCounts[hospIdx]
    end
    return caseCountMatrix
end


end  # module HospitalData

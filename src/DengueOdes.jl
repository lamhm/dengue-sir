module DengueOdes

include("globaldef.jl")

using ..Util
using ..ModelConfigs

using DifferentialEquations


struct SIRColumnIndexer
    "Total number of columns."
    nColumns::Int

    "Column index of the primary susceptible groups."
    priSusceptbIdx::Int

    """
    Column indices of the primary infected groups.
    Each index corresponds to a serotype.
    """
    priInfectedIdxs::Vector{Int}

    """
    Column indices of the primary recovered groups. This is a matrix,
    of which the row indices correspond to the serotypes and the column indices
    correspond to the sub-compartments of the primary recovered class.
    Example: `priRecovredIdxs[1, 2]` stores the column index of the
    sub-compartment 2 of the groups of the population who are recovered from a
    primary infection caused by the serotype 1.
    """
    priRecovredIdxs::Matrix{Int}

    """
    Column indices of the secondary susceptible groups.
    Each index corresponds to a serotype.
    """
    sndSusceptbIdxs::Vector{Int}

    """
    Column indices of the seconary infected groups.
    Each index corresponds to a serotype.
    """
    sndInfectedIdxs::Vector{Int}

    """
    Column indices of the population groups that are secondary infected while
    they are still cross-protected (cpr) from symptoms.
    Each index corresponds to a serotype.
    """
    cprInfectedIdxs::Vector{Int}

    "Column index of the secondary recovered group."
    sndRecoverdIdx::Int

    function SIRColumnIndexer(nSerotypes::Int, nCrossProtectSubComps::Int)
        nColumns = 2 +                  # S₀ + R₂
                   nSerotypes * 4 +     # I₁ + S₁ + I₂ + I₂ᵖ
                   nSerotypes * nCrossProtectSubComps  # R₁

        priSusceptbIdx = 1
        priInfectedIdxs = [i + 1 for i = 1 : nSerotypes]

        priRecovredIdxs = zeros(Int, nSerotypes, nCrossProtectSubComps)
        lastIdx = 1 + nSerotypes  # S₀ + I₁
        for row = 1 : nSerotypes, col = 1 : nCrossProtectSubComps
            lastIdx += 1
            priRecovredIdxs[row, col] = lastIdx
        end

        lastIdx = 1 + nSerotypes + nSerotypes * nCrossProtectSubComps
        sndSusceptbIdxs = [i + lastIdx for i = 1 : nSerotypes]

        lastIdx += nSerotypes
        sndInfectedIdxs = [i + lastIdx for i = 1 : nSerotypes]

        lastIdx += nSerotypes
        cprInfectedIdxs = [i + lastIdx for i = 1 : nSerotypes]

        lastIdx += nSerotypes
        sndRecoverdIdx = lastIdx + 1

        new( nColumns,
             priSusceptbIdx,
             priInfectedIdxs,
             priRecovredIdxs,
             sndSusceptbIdxs,
             sndInfectedIdxs,
             cprInfectedIdxs,
             sndRecoverdIdx )
    end

    SIRColumnIndexer(modelVars::ModelVars) = SIRColumnIndexer(
        modelVars.nSerotypes,
        modelVars.nCrossProtectSubComps
    )
end # struct



"""
Generate the initial conditions for the ODE solver based on the given model
conditions.
"""
function first_sys_state(modelVars::ModelVars;
                         totalTranRate::Float = 1.6,
                         recoveryRate::Float  = 1.0 / 7.0,
                         crossProtectDuration::Float = 120.0
                        ) :: Matrix{Float}

    colIndexer = SIRColumnIndexer(modelVars)

    nSerotypes = modelVars.nSerotypes
    nCrossProtectSubComps = modelVars.nCrossProtectSubComps
    serotypeTranRates = [totalTranRate / nSerotypes for i = 1 : nSerotypes]

    totalPopSize = modelVars.initPopSize

    nAgeGroups = length(modelVars.ageGroupSizes)
    maxSimAge = sum(modelVars.ageGroupSizes)

    proportionOfAgeGroups =
        [ageGroupSize / maxSimAge for ageGroupSize = modelVars.ageGroupSizes]
    popSizeOfAgeGroups = proportionOfAgeGroups * totalPopSize
    unassignedPops = popSizeOfAgeGroups

    #=
    Approximated force of infection:
    FOI = I * tranRate
        ≈ tranRate * (deathRate / (recoveryRate + deathRate)) - death_rate
    =#
    deathRate  = 1.0 / maxSimAge
    priFois = serotypeTranRates *
           (deathRate / (recoveryRate + deathRate)) .- deathRate
    for i = 1 : nSerotypes
        if priFois[i] < 0.0
            priFois[i] = 0.0
        end
    end

    totalPriFoi = sum(priFois)
    if totalPriFoi <= 0.0
        throw(
            DomainError(totalTranRate,
                        "transmission rate is too low to sustain epidemics.")
        )
    end

    nInfectedPerSerotype = priFois ./ serotypeTranRates * totalPopSize
    nInfectedTotal = sum(nInfectedPerSerotype)
    serotypeProportions = nInfectedPerSerotype / nInfectedTotal

    avgAgeOfGroups = zeros(nAgeGroups)

    prevAge = 0.0
    for ageGroup = 1 : nAgeGroups
        avgAgeOfGroups[ageGroup] =
            modelVars.ageGroupSizes[ageGroup] / 2.0 + prevAge
        prevAge += modelVars.ageGroupSizes[ageGroup]
    end

    sysState = zeros(Float, nAgeGroups, colIndexer.nColumns)

    # S₀: primary susceptible
    nPriSusceptb = popSizeOfAgeGroups .* exp.(-totalPriFoi * avgAgeOfGroups)
    unassignedPops -= nPriSusceptb
    sysState[:, colIndexer.priSusceptbIdx] = nPriSusceptb

    # I₁: primary infected
    nUnassignedInfected = nInfectedPerSerotype
    for ageGroup = 1 : nAgeGroups
        if ageGroup == nAgeGroups
            nExpectedInfected = sum(nUnassignedInfected)
        else
            nExpectedInfected = nInfectedTotal * proportionOfAgeGroups[ageGroup]
        end
        if nExpectedInfected < 0
            nExpectedInfected = 0
        elseif nExpectedInfected > unassignedPops[ageGroup]
            nExpectedInfected = unassignedPops[ageGroup]
        end

        unassignedPops[ageGroup] -= nExpectedInfected
        nPriInfected = nExpectedInfected * serotypeProportions
        nUnassignedInfected -= nPriInfected
        sysState[ageGroup, colIndexer.priInfectedIdxs] = nPriInfected
    end

    # R₁: primary recovered (and cross-protected)
    nPriRecovered =
        unassignedPops .* exp.(-(1.0 / crossProtectDuration) * avgAgeOfGroups)
    unassignedPops -= nPriRecovered

    for serotype = 1 : nSerotypes
        nCprSubComp = nPriRecovered * serotypeProportions[serotype] /
                      nCrossProtectSubComps
        for subCompIdx = 1 : nCrossProtectSubComps
            sysState[:, colIndexer.priRecovredIdxs[serotype, subCompIdx]] =
                nCprSubComp
        end
    end

    # S₁: secondary susceptible
    infectedDuration = 1.0 / recoveryRate
    sndSusceptibleDuration =
        avgAgeOfGroups .- (crossProtectDuration + infectedDuration)
    totalFoiOfOtherSTypes = totalPriFoi .- priFois

    nSndSusceptb = zeros(nAgeGroups)
    for ageGroup = 1 : nAgeGroups
        if sndSusceptibleDuration[ageGroup] <= 0.0
            nSndSusceptb[ageGroup] = unassignedPops[ageGroup]
        else
            nSndSusceptb[ageGroup] = unassignedPops[ageGroup] *
                sum(exp.(
                    -totalFoiOfOtherSTypes * sndSusceptibleDuration[ageGroup]
                ))
        end

        if nSndSusceptb[ageGroup] > unassignedPops[ageGroup]
            nSndSusceptb[ageGroup] = unassignedPops[ageGroup]
        end
    end

    unassignedPops -= nSndSusceptb
    sysState[:, colIndexer.sndSusceptbIdxs] =
        nSndSusceptb * transpose(serotypeProportions)

    # I₂: secondary infected
    for ageGroup = 1 : nAgeGroups
        if nSndSusceptb[ageGroup] > 0.0
            # Divide infected individuals into I₁ and I₂
            sysState[ageGroup, colIndexer.sndInfectedIdxs] =
                sysState[ageGroup, colIndexer.priInfectedIdxs] / 2.0
            sysState[ageGroup, colIndexer.priInfectedIdxs] =
                sysState[ageGroup, colIndexer.sndInfectedIdxs]
        end
    end

    # R₂: secondary recovered
    sysState[:, colIndexer.sndRecoverdIdx] = unassignedPops

    return sysState
end # function



function intepret_ode_time(odeTime::Float, vars::ModelVars) :: Tuple{Int, Float}
    odeYear       = Int(ceil(odeTime / vars.nDaysPerYear))
    odeYearIdx    = Int(odeYear - vars.nOdeWarmupYears)
    odeTimeOfYear = mod(odeTime, vars.nDaysPerYear)
    if odeTimeOfYear <= 0.0
        odeTimeOfYear = vars.nDaysPerYear
    end
    return (odeYearIdx, odeTimeOfYear)
end



"""
    pri_fois(sysState::Matrix{Float},
             totalPopSize::Float,
             colIndexer::SIRColumnIndexer,
             vars::ModelVars,
             params::ModelParams
             odeTime::Float) :: Vector{Float}
Calculate the population size and the primary force of infection of each
serotype, given the current state of the system.
"""
function pri_fois(sysState::Matrix{Float},
                  totalPopSize::Float,
                  colIndexer::SIRColumnIndexer,
                  vars::ModelVars,
                  params::ModelParams,
                  odeTime::Float) :: Vector{Float}

    odeYearIdx, odeTimeOfYear = intepret_ode_time(odeTime, vars)
    tranSeasonLength  = vars.nDaysPerYear / vars.nSeasonalTranPeriods
    tranSeasonIdx     = Int(ceil(odeTimeOfYear / tranSeasonLength))
    seasonalTranScale = params.seasonalTranScales[tranSeasonIdx]

    serotypeTranRates = params.serotypeTranRates
    sndInfectivity    = params.sndInfectivity

    tranRates = serotypeTranRates * seasonalTranScale
    totalInfected =
        sum(sysState[:, colIndexer.priInfectedIdxs], dims = 1) +
        sndInfectivity * sum(sysState[:, colIndexer.sndInfectedIdxs], dims = 1) +
        sndInfectivity * sum(sysState[:, colIndexer.cprInfectedIdxs], dims = 1)
    totalInfected = totalInfected[1, :]
    priFois = (tranRates .* totalInfected) / totalPopSize
    return priFois
end



"""
The ODE system of the SIR model. This function should only by used by the ODE
solver.
"""
function ode_system(diffs::Matrix{Float},
                    sysState::Matrix{Float},
                    modelConfig::Tuple{ModelVars, ModelParams},
                    odeTime::Float)

    vars, params = modelConfig
    colIndexer = SIRColumnIndexer(vars)

    nSerotypes = vars.nSerotypes
    nAgeGroups = length(vars.ageGroupSizes)

    # Calculate seasonal birth rate
    odeYearIdx, odeTimeOfYear = intepret_ode_time(odeTime, vars)
    birthSeasonLength = vars.nDaysPerYear / length(vars.seasonalBirthRates)
    birthSeasonIdx    = Int(ceil(odeTimeOfYear / birthSeasonLength))
    seasonalBirthRate = length(vars.seasonalBirthRates) *
                        vars.seasonalBirthRates[birthSeasonIdx] /
                        sum(vars.seasonalBirthRates)

    # Calculate population growth
    instantNatPopGrowthRate = 0.0
    instantMigrationRate    = 0.0
    if odeYearIdx >= 1 && odeYearIdx <= vars.nYearsToFit
        instantNatPopGrowthRate =
            log(vars.annualNatPopGrowthRates[odeYearIdx]) / vars.nDaysPerYear
        instantMigrationRate =
            log(1.0 + vars.annualNetMigRates[odeYearIdx]) / vars.nDaysPerYear
    else
        if length(vars.annualNatPopGrowthRates) > 0
            instantNatPopGrowthRate =
                log(minimum(vars.annualNatPopGrowthRates)) / vars.nDaysPerYear
        end
    end

    Util.zerofy_negative_numbers!(sysState)
    S0   = sysState[:, colIndexer.priSusceptbIdx]
    I1   = sysState[:, colIndexer.priInfectedIdxs]
    S1   = sysState[:, colIndexer.sndSusceptbIdxs]
    I2   = sysState[:, colIndexer.sndInfectedIdxs]
    Icpr = sysState[:, colIndexer.cprInfectedIdxs]
    R2   = sysState[:, colIndexer.sndRecoverdIdx]

    # Population size and FOIs
    totalPopSize = sum(sysState)
    priFois = pri_fois(sysState, totalPopSize,
                       colIndexer, vars, params, odeTime)
    totalPriFoi = sum(priFois)
    totalSndFoi = totalPriFoi .- priFois
    sndFois = repeat(transpose(priFois), nSerotypes, 1)
    sndFois[1 : nSerotypes + 1 : end] .= 0  # make the diagonal become zeros

    # Derivatives of S0
    nOldestIndv = sum(sysState[nAgeGroups, :])
    nAdditionalBirths =
        instantNatPopGrowthRate * totalPopSize * seasonalBirthRate
    nBirths = nAdditionalBirths +
              nOldestIndv * seasonalBirthRate / vars.ageGroupSizes[nAgeGroups]
    nAddedByAging = vcat(
        nBirths,
        (S0[1 : end-1]  * seasonalBirthRate) ./ vars.ageGroupSizes[1 : end-1]
    )
    nRemovedByAging = S0 * seasonalBirthRate ./ vars.ageGroupSizes
    nRemovedByInfection = S0 * totalPriFoi
    diffs[:, colIndexer.priSusceptbIdx] =
        nAddedByAging - nRemovedByAging - nRemovedByInfection +
        (S0 * instantMigrationRate)

    # Derivatives of I1
    nAddedByAging = vcat(
        transpose([0.0 for i = 1 : nSerotypes]),
        I1[1 : end-1, :] * seasonalBirthRate ./ vars.ageGroupSizes[1 : end-1]
    )
    nRemovedByAging = I1 * seasonalBirthRate ./ vars.ageGroupSizes
    nAddedByInfection  = S0 * transpose(priFois)
    nRemovedByRecovery = I1 * params.recoveryRate
    diffs[:, colIndexer.priInfectedIdxs] =
            nAddedByAging - nRemovedByAging +
            nAddedByInfection - nRemovedByRecovery +
            (I1 * instantMigrationRate)

    # nInfectedDuringCpr stores the number of infections occuring in protected
    # individuals. Each row of this matrix corresponds to an age group. Each
    # column corresponds to a secondary serotype.
    nInfectedDuringCpr = zeros(Float, nAgeGroups, nSerotypes)

    # Derivatives of R1
    partialCrossProtectWaningRate =
        vars.nCrossProtectSubComps / params.crossProtectDuration
    infectionRisk = 1.0 - params.crossProtectStrength
    prevEpiClass = I1
    for cprSubCompIdx = 1 : vars.nCrossProtectSubComps
        subCompColIdxs = colIndexer.priRecovredIdxs[:, cprSubCompIdx]
        currentSubComp = sysState[:, subCompColIdxs]

        nAddedByAging = vcat(
            transpose([0.0 for i = 1 : nSerotypes]),
            currentSubComp[1 : end-1, :] * seasonalBirthRate ./
                vars.ageGroupSizes[1 : end-1]
        )
        nRemovedByAging = currentSubComp * seasonalBirthRate ./ vars.ageGroupSizes

        immuneShiftingRate = params.recoveryRate
        if cprSubCompIdx > 1
            immuneShiftingRate = partialCrossProtectWaningRate
        end
        nAddedByImmuneShift = prevEpiClass * immuneShiftingRate
        nRemovedByImmuneShift = currentSubComp * partialCrossProtectWaningRate
        nRemovedByInfection =
            infectionRisk * currentSubComp .* transpose(totalSndFoi)

        diffs[:, subCompColIdxs] =
            nAddedByAging - nRemovedByAging - nRemovedByInfection +
            nAddedByImmuneShift - nRemovedByImmuneShift +
            (currentSubComp * instantMigrationRate)

        nInfectedDuringCpr += infectionRisk * currentSubComp * sndFois
        prevEpiClass = currentSubComp
    end

    # Derivatives of S1
    nAddedByAging = vcat(
        transpose([0.0 for i = 1 : nSerotypes]),
        S1[1 : end-1, :] * seasonalBirthRate ./ vars.ageGroupSizes[1 : end-1]
    )
    nRemovedByAging = S1 * seasonalBirthRate ./ vars.ageGroupSizes
    nAddedByImmuneShift  = prevEpiClass * partialCrossProtectWaningRate
    nRemovedByInfection = S1 .* transpose(totalSndFoi)
    diffs[:, colIndexer.sndSusceptbIdxs] =
        nAddedByAging - nRemovedByAging +
        nAddedByImmuneShift - nRemovedByInfection +
        (S1 * instantMigrationRate)

    # Derivatives of I2
    nAddedByAging = vcat(
        transpose([0.0 for i = 1 : nSerotypes]),
        I2[1 : end-1, :] * seasonalBirthRate ./ vars.ageGroupSizes[1 : end-1]
    )
    nRemovedByAging = I2 * seasonalBirthRate ./ vars.ageGroupSizes
    nAddedByInfection  = S1 * sndFois
    nRemovedByRecovery = I2 * params.recoveryRate
    diffs[:, colIndexer.sndInfectedIdxs] =
            nAddedByAging - nRemovedByAging +
            nAddedByInfection - nRemovedByRecovery +
            (I2 * instantMigrationRate)

    # Derivatives of Icpr (infected while cross-protected)
    nAddedByAging = vcat(
        transpose([0.0 for i = 1 : nSerotypes]),
        Icpr[1 : end-1, :] * seasonalBirthRate ./ vars.ageGroupSizes[1 : end-1]
    )
    nRemovedByAging = Icpr * seasonalBirthRate ./ vars.ageGroupSizes
    nRemovedByRecovery = Icpr * params.recoveryRate
    diffs[:, colIndexer.cprInfectedIdxs] =
            nAddedByAging - nRemovedByAging +
            nInfectedDuringCpr - nRemovedByRecovery +
            (Icpr * instantMigrationRate)

    # Derivatives of R2
    nAddedByAging = vcat(
        0.0,
        R2[1 : end-1] * seasonalBirthRate ./ vars.ageGroupSizes[1 : end-1]
    )
    nRemovedByAging = R2 * seasonalBirthRate ./ vars.ageGroupSizes
    nAddedByRecovery =
        sum((Icpr + I2) * params.recoveryRate, dims = 2)
    diffs[:, colIndexer.sndRecoverdIdx] =
        nAddedByAging - nRemovedByAging + nAddedByRecovery +
        (R2 * instantMigrationRate)
end # function



"""
This function is only used by the ODE solver to force all negative solutions to
be rejected.
"""
function is_sys_state_negative(sysState::Matrix{Float},
                               modelConfig::Tuple{ModelVars, ModelParams},
                               odeTime::Float) :: Bool
    return any(sysState .< 0.0)
end



function init_sys_state(vars::ModelVars,
                        initParams::ModelParams;
                        nCalibratingYears::Float = 97.0,
                        odeAbsTolerance::Float   = 1e-6,
                        odeRelTolerance::Float   = 1e-4) :: Matrix{Float}

    initPopSize = vars.initPopSize
    if length(vars.annualNatPopGrowthRates) > 0 && vars.nOdeWarmupYears > 0
        minNatPopGrowthRate = minimum(vars.annualNatPopGrowthRates)
        initPopSize = vars.initPopSize /
                      (minNatPopGrowthRate ^ vars.nOdeWarmupYears)
    end

    initVars = ModelVars(
        vars.ageGroupSizes,
        vars.nDaysPerYear,
        0.0,  # nYearsToFit
        0.0,  # nDaysReportingLag
        vars.minAcceptAgeInDays,
        vars.maxAcceptAgeInDays,
        vars.nDaysPerSummaryPeriod,
        vars.maxConsecutiveZeroDays,
        vars.movingAvgWindowSize,
        vars.movingAvgWeight,
        initPopSize,
        [],  # annualNatPopGrowthRates
        [],  # annualNetMigRates
        vars.seasonalBirthRates,
        vars.nSerotypes,
        vars.nSeasonalTranPeriods,
        vars.hospRateCutoffs,
        vars.startYearOfAvailFOI,
        vars.nYearsOfAvailFOI,
        vars.availFOI,
        vars.nCrossProtectSubComps,
        nCalibratingYears,  # nOdeWarmupYears
        odeAbsTolerance,
        odeRelTolerance
    )

    firstSysState = first_sys_state(initVars)
    odeEndTime = nCalibratingYears * initVars.nDaysPerYear
    timeSpan = (0.0, odeEndTime)

    odeProblem = ODEProblem(
        ode_system, firstSysState, timeSpan, (initVars, initParams)
    )
    odeSolution = solve(odeProblem,
                        OwrenZen5(),
                        abstol = initVars.odeAbsTolerance,
                        reltol = initVars.odeRelTolerance,
                        saveat = [odeEndTime],
                        # alg_hints = [:nonstiff],
                        isoutofdomain = is_sys_state_negative)

    initSysState = odeSolution.u[1]
    Util.zerofy_negative_numbers!(initSysState)

    return initSysState
end # function



function redist_column_proportion!(matrix::Matrix{Float},
                                   columnIdxs::Vector{Int},
                                   proportions::Vector{Float})
    colSums = sum(matrix[:, columnIdxs], dims = 2)
    matrix[:, columnIdxs] = colSums .* transpose(proportions)
end # function



function redistribute_serotypes!(odeState::Matrix{Float},
                                 modelVars::ModelVars,
                                 serotypeProportions::Vector{Float})
    colIndexer = SIRColumnIndexer(modelVars)
    serotypeProportions = serotypeProportions ./ sum(serotypeProportions)

    redist_column_proportion!(
        odeState, colIndexer.priInfectedIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.cprInfectedIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.sndInfectedIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.sndSusceptbIdxs, serotypeProportions
    )

    for subcomp = 1 : modelVars.nCrossProtectSubComps
        priRecovredColIdxs = colIndexer.priRecovredIdxs[:, subcomp]
        redist_column_proportion!(
            odeState, priRecovredColIdxs, serotypeProportions
        )
    end
end



function solve_odes(vars::ModelVars,
                    params::ModelParams,
                    odeInitState::Matrix{Float}) :: ODESolution
    odeStartDay = vars.nOdeWarmupYears * vars.nDaysPerYear +
                  1.0 - vars.nDaysReportingLag
    odeStartDay = odeStartDay < 0.0 ? 0.0 : odeStartDay
    odeEndDay = ceil(vars.nYearsToFit * vars.nDaysPerYear) + odeStartDay - 1.0
    odeEndDay = odeEndDay < odeStartDay ? odeStartDay : odeEndDay
    daysToSave = [t for t = odeStartDay : 1.0 : odeEndDay]
    timeSpan = (0.0, odeEndDay)

    odeProblem = ODEProblem(ode_system, odeInitState, timeSpan, (vars, params))
    odeSolution = solve(odeProblem,
                        OwrenZen3(),
                        # OwrenZen5(),
                        # TRBDF2(),
                        abstol = vars.odeAbsTolerance,
                        reltol = vars.odeRelTolerance,
                        saveat = daysToSave,
                        # alg_hints = [:nonstiff],
                        isoutofdomain = is_sys_state_negative)
    return odeSolution
end # function


function compute_incidences(odeSolution::ODESolution,
                            vars::ModelVars,
                            params::ModelParams,
                            nTimePointsPerSummary::Int
                            ) :: Tuple{Matrix{Float}, Matrix{Float}}
    colIndexer = SIRColumnIndexer(vars)
    nAgeGroups  = length(vars.ageGroupSizes)
    nTimePoints = length(odeSolution.u)

    priIncidences = Matrix{Float}(undef, nAgeGroups, nTimePoints)
    sndIncidences = Matrix{Float}(undef, nAgeGroups, nTimePoints)
    for timePoint = 1 : nTimePoints
        odeTime = odeSolution.t[timePoint]
        sysState = odeSolution.u[timePoint]
        Util.zerofy_negative_numbers!(sysState)

        popSize = sum(sysState)
        priFois = pri_fois(sysState, popSize,
                           colIndexer, vars, params, odeTime)
        totalPriFoi = sum(priFois)
        totalSndFoi = totalPriFoi .- priFois

        nPriSusceptibles = sysState[:, colIndexer.priSusceptbIdx]
        priIncidences[:, timePoint] = nPriSusceptibles * totalPriFoi

        nSndSusceptibles = sysState[:, colIndexer.sndSusceptbIdxs]
        sndIncidences[:, timePoint] =
            sum(nSndSusceptibles .* transpose(totalSndFoi), dims = 2)
    end

    if nTimePointsPerSummary > 1
        priIncidences =
            Util.group_matrix_columns(priIncidences, nTimePointsPerSummary)
        sndIncidences =
            Util.group_matrix_columns(sndIncidences, nTimePointsPerSummary)
    end

    return (priIncidences, sndIncidences)
end # function



function compute_seroprevalence(odeSolution::ODESolution,
                                vars::ModelVars) :: Vector{Float}
    colIndexer = SIRColumnIndexer(vars)
    nAgeGroups  = length(vars.ageGroupSizes)
    nOdeTimePoints = length(odeSolution.u)

    startTimePointOfAvailFOI = floor(
        Int,
        (vars.startYearOfAvailFOI - 1.0) * vars.nDaysPerYear
    ) + 1
    startTimePointOfAvailFOI = max(startTimePointOfAvailFOI, 1)

    endTimePointOfAvailFOI = ceil(
        Int,
        (vars.startYearOfAvailFOI + vars.nYearsOfAvailFOI) * vars.nDaysPerYear
    )
    endTimePointOfAvailFOI = min(endTimePointOfAvailFOI, nOdeTimePoints)

    isFoiAvailable = !isnan(vars.availFOI) &&
                     startTimePointOfAvailFOI <= endTimePointOfAvailFOI

    seroprevalence = zeros(Float, nAgeGroups)
    if isFoiAvailable
        for timePoint = startTimePointOfAvailFOI : endTimePointOfAvailFOI
            sysState = odeSolution.u[timePoint]
            nPriSusceptibles = sysState[:, colIndexer.priSusceptbIdx]

            # The next sum(..) returns a matrix, the [..] turns it into a vector
            popSizeOfAgeGroups = sum(sysState, dims = 2)[:, 1]
            seroprevalence += 1.0 .- (nPriSusceptibles ./ popSizeOfAgeGroups)
        end

        nTimePointsOfAvailFOI =
            endTimePointOfAvailFOI - startTimePointOfAvailFOI + 1
        seroprevalence = seroprevalence ./ nTimePointsOfAvailFOI
        for ageGroup = 1 : nAgeGroups
            if seroprevalence[ageGroup] < 1e-4
                seroprevalence[ageGroup] = 1e-4
            end
            if seroprevalence[ageGroup] > 0.9999
                seroprevalence[ageGroup] = 0.9999
            end
        end
    else
        seroprevalence .= NaN
    end

    return seroprevalence
end # function



function serialise_ode_sol(odeSolution::ODESolution,
                           recordingTimeStep::Int) :: Matrix{Float}
    nOdeTimePoints = length(odeSolution.u)
    nAgeGroups, nOdeClasses = size(odeSolution.u[1])
    nRecordedTimes = floor(Int, nOdeTimePoints / recordingTimeStep)

    result = Matrix{Float}(undef, nAgeGroups, nRecordedTimes * nOdeClasses)
    lastSavedColumn = 0
    for odeTimePoint = 1 : recordingTimeStep : nOdeTimePoints
        result[:, (lastSavedColumn + 1) : (lastSavedColumn + nOdeClasses)] =
            odeSolution.u[odeTimePoint]
        lastSavedColumn += nOdeClasses
    end

    return result
end



function deserialise_ode_sol(serialOdeStates::Matrix{Float},
                             vars::ModelVars) :: Array{Float, 3}
    colIndexer = SIRColumnIndexer(vars)
    nOdeClasses = colIndexer.nColumns
    nAgeGroups, nSerialisedCols = size(serialOdeStates)
    nTimePoints = div(nSerialisedCols, nOdeClasses)

    result = Array{Float, 3}(undef, nTimePoints, nAgeGroups, nOdeClasses)
    processedCol = 0
    for timePoint = 1 : nTimePoints
        result[timePoint, :, :] =
            serialOdeStates[:, (processedCol + 1) : (processedCol + nOdeClasses)]
        processedCol += nOdeClasses
    end
    return result
end



function extract_ode_class(odeSolution::ODESolution,
                           odeClassIdx::Int,
                           recordingTimeStep::Int) :: Matrix{Float}
    nOdeTimePoints = length(odeSolution.u)
    nAgeGroups, nOdeClasses = size(odeSolution.u[1])
    nRecordedTimes = floor(Int, nOdeTimePoints / recordingTimeStep)

    result = Matrix{Float}(undef, nAgeGroups, nRecordedTimes)
    recordedTime = 0
    for odeTime = 1 : recordingTimeStep : nOdeTimePoints
        recordedTime += 1
        result[:, recordedTime] = odeSolution.u[odeTime][:, odeClassIdx]
    end
    return result
end


end  # module DengueOdes

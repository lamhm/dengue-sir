module ModelConfigs

include("globaldef.jl")

import YAML



"List of all model variables"
struct ModelVars
    ageGroupSizes::Vector{Float}

    nDaysPerYear::Float
    nYearsToFit::Float
    nDaysReportingLag::Float

    """
    The minimum and maximum age (in days) for the cases to be included as the
    input data for the model fitting. Any cases whose ages are out of this
    specified range will be ignored when loading the data set.
    """
    minAcceptAgeInDays::Float
    maxAcceptAgeInDays::Float

    nDaysPerSummaryPeriod::Int
    maxConsecutiveZeroDays::Int
    movingAvgWindowSize::Int
    movingAvgWeight::Float

    initPopSize::Float

    """
    Annual natural population growth rates.
    Natural growth rate = 1.0 means number of births = number of deaths.
    N.B. For almost every area of Vietnam, these rates have been always >1.
    """
    annualNatPopGrowthRates::Vector{Float}

    """
    Annual net migration rates:
        = 0.0 : immigrations and emigrations were equal.
        < 0.0 : immigrations < emigrations
        > 0.0 : immigrations > emigrations
    """
    annualNetMigRates::Vector{Float}

    seasonalBirthRates::Vector{Float}

    nSerotypes::Int
    nSeasonalTranPeriods::Int

    "Age cut-offs for age-specific hospitalisation rates"
    hospRateCutoffs::Vector{Float}

    "The starting year of which the data of FOI is available"
    startYearOfAvailFOI::Float

    "The number years of which the data of FOI is available"
    nYearsOfAvailFOI::Float

    "The annual force of infection (FOI) of the years above"
    availFOI::Float

    "Number of sub-compartments for the cross-protected classes"
    nCrossProtectSubComps::Int

    nOdeWarmupYears::Int
    odeAbsTolerance::Float
    odeRelTolerance::Float
end # ModelVars
export ModelVars


function validate!(vars::ModelVars)
    if vars.nSerotypes < 1 || vars.nSerotypes > 4
        throw(ErrorException(
            "the number of serotypes must be between 1 and 4."
        ))
    end

    sumSeasonalBirthRates = sum(vars.seasonalBirthRates)
    if sumSeasonalBirthRates != 1.0
        for i in 1 : length(vars.seasonalBirthRates)
            vars.seasonalBirthRates[i] /= sumSeasonalBirthRates
        end
    end

    if vars.nYearsToFit > length(vars.annualNatPopGrowthRates)
        throw(DomainError(
            vars.nYearsToFit,
            "not enough data for the natural population growth rate " *
            "(required: $(vars.nYearsToFit) years, " *
            "received: $(length(vars.annualNatPopGrowthRates)) years)."
        ))
    end

    if vars.nYearsToFit > length(vars.annualNetMigRates)
        throw(DomainError(
            vars.nYearsToFit,
            "not enough data for the net migration rate " *
            "(required: $(vars.nYearsToFit) years, " *
            "received: $(length(vars.annualNetMigRates)) years)."
        ))
    end

    if !all(0.9 .<= vars.annualNatPopGrowthRates .<= 1.1)
        throw(DomainError(
            vars.annualNatPopGrowthRates,
            "the annual natural population growth rates are not in the " *
            "expected range, i.e. [0.9, 1.1]."
        ))
    end

    if !all(-0.1 .<= vars.annualNetMigRates .<= 0.1)
        throw(DomainError(
            vars.annualNetMigRates,
            "the annual net migration rates are not in the expected " *
            "range, i.e. [-0.1, 0.1]."
        ))
    end
end # function


"""
List of all model parameters. Technically, the values of these parameters can be
altered during the model fitting process.
"""
struct ModelParams
    serotypeTranRates::Vector{Float}
    seasonalTranScales::Vector{Float}

    recoveryRate::Float

    "The waning rate of the cross-immunity induced by a primary infection"
    crossProtectDuration::Float

    """
    Cross immunity (induced by a primary infection) can protect an individual
    from 2 things: infection and/or symptoms. In this model, cross-protected
    individuals are always asymptomatic (if they are infected). However,
    depending on `crossproctectionstrength`, they may fully
    (`crossProtectStrength = 1`) or partially
    (`0 < crossProtectStrength < 1`) be protected from infection.
    The rate that a cross-protected individual becomes infected is
    `(1.0 - crossProtectStrength) × λ`, where `λ` is the force of infection.
    """
    crossProtectStrength::Float

    hospRates::Vector{Float}
    # hospRateUpBound::Float
    # hospRateLowScaling::Float
    # hospRateSlope::Float
    # hospRateDayOfMidpoint::Float

    adeEffectScale::Float
    adeAgeAvg::Float
    adeAgeVar::Float

    maternalProtectDuration::Float

    sndSymptomaticProb::Float
    priSymptomaticProb::Float

    """
    The infectivity of secondary infectious individuals as compared to primary
    cases. Generally, this number is thought to be >1.0. However, when this
    number is greater than 1.85, the system dynamic may become chaotic.
    """
    sndInfectivity::Float
end # struct
export ModelParams


function validate!(params::ModelParams, vars::ModelVars)
    # sumSeasonalTranScales = sum(params.seasonalTranScales)
    # if sumSeasonalTranScales != 1.0
    #     for i in 1 : length(params.seasonalTranScales)
    #         params.seasonalTranScales[i] /= sumSeasonalTranScales
    #     end
    # end

    if params.seasonalTranScales[1] != 1.0
        params.seasonalTranScales ./= params.seasonalTranScales[1]
        # throw(DomainError(
        #     params.seasonalTranScales,
        #     "the 1st seasonal transmission scale must always be 1.0."
        # ))
    end

    if any(params.seasonalTranScales .<= 0.0)
        throw(DomainError(
            params.seasonalTranScales,
            "seasonal transmission scales must be positive."
        ))
    end

    if params.crossProtectStrength < 0.0 || params.crossProtectStrength > 1.0
        throw(DomainError(
            params.crossProtectStrength,
            "cross-protection strength must be in [0, 1] range."
        ))
    end

    if vars.nSerotypes != length(params.serotypeTranRates)
        throw(ErrorException(
            "the number of serotypes does not match the number of " *
            "serotype-specific transmisison rates."
        ))
    end

    if vars.nSeasonalTranPeriods != length(params.seasonalTranScales)
        throw(ErrorException(
            "the number of transmission periods per year does not match the " *
            "number of period-specific transmisison scales."
        ))
    end

    if length(vars.hospRateCutoffs) != length(params.hospRates)
        throw(ErrorException(
            "the number of age cut-offs for hospitalisation rates does not " *
            "match the number of age-specific hospitalisation rates."
        ))
    end

end # function



"""
    parse_yml_data(outType::Type, ymlData::Dict)

Parse YAML data (stored in a `Dict` object) into a given output type.
"""
function parse_yml_data(outType::Type, ymlData::Dict)
    # Helper functions
    format_data(type::Type{T}, value::Number) where T<:Number =
        convert(type, value)
    format_data(type::Type{String}, value) = string(value)
    format_data(type::Type{T}, value::String) where T<:Number =
        format_data(type, eval(Meta.parse(value)))

    # The parsing process starts here
    objData = Vector{Any}()
    for field in fieldnames(outType)
        fieldType = fieldtype(outType, field)
        fieldName = string(field)
        if !(fieldName in keys(ymlData))
            throw(ErrorException(
                "'$(fieldName)' is not found in the configuration data for " *
                "'$(outType)'."
            ))
        end
        ymlDataOfField = ymlData[fieldName]

        if fieldType <: Vector
            elementType = eltype(fieldType)
            fieldData = Vector{elementType}()
            for elementData in ymlDataOfField
                formattedData = format_data(elementType, elementData)
                push!(fieldData, formattedData)
            end
        elseif fieldType <: Number || fieldType <: String
            fieldData = format_data(fieldType, ymlDataOfField)
        elseif isstructtype(fieldType)
            fieldData = parse_yml_data(fieldType, ymlDataOfField)
        end
        push!(objData, fieldData)
    end
    return (eval(outType))(objData...)
end # function



"""
    load_model_config(ymlFile::String) :: ModelConfig

Load model configuration from a YAML file.
"""
function load_model_config(ymlFile::String) :: Tuple{ModelVars, ModelParams}
    configData = YAML.load_file(ymlFile)

    modelVars = parse_yml_data(ModelVars, configData["vars"])
    modelParams = parse_yml_data(ModelParams, configData["params"])

    validate!(modelVars)
    validate!(modelParams, modelVars)

    return (modelVars, modelParams)
end # function
export load_model_config


end  # module ModelConfigs

module CustomDistributions

import Distributions
import Mamba


struct FixedValDist{T<:Real} <: Distributions.ContinuousUnivariateDistribution
    value::T
end

Distributions.logpdf(d::FixedValDist{T}, x::T) where T<:Real = 0.0
Distributions.minimum(d::FixedValDist) = -Inf
Distributions.maximum(d::FixedValDist) = Inf
Mamba.rand(d::FixedValDist) = d.value

end  # module CustomDistributions

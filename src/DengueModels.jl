module DengueModels

include("globaldef.jl")

using ..HospitalData
using ..ModelConfigs

export age_specific_hosp_rates
export maternal_effects
export calculate_hospitalisations



# function hospitalisation_rates(ageGroupSizes::Vector{Float},
#                                upBound::Float,
#                                lowBoundScaling::Float,
#                                slope::Float,
#                                dayOfMidPoint::Float,
#                                nBinsPerAgeGroup::Int = 10
#                                ) :: Vector{Float}
#
#     nAgeGroups = length(ageGroupSizes)
#     hospRates = zeros(Float, nAgeGroups)
#     binSizes = ageGroupSizes / (nBinsPerAgeGroup + 1.0)
#
#     age = 0.0
#     for ageGroup = 1 : nAgeGroups
#         hospRates[ageGroup] = 0.0
#         age += binSizes[ageGroup]
#
#         for bin = 1 : nBinsPerAgeGroup
#             hospRates[ageGroup] += upBound *
#                 ( 1.0 - ( (1.0 - lowBoundScaling) /
#                           (1.0 + exp(-slope * (age - dayOfMidPoint))) )
#                 )
#             age += binSizes[ageGroup]
#         end
#     end
#     hospRates ./= nBinsPerAgeGroup
#     return hospRates
# end # function



"""
Calculate the age-specific hospitalisation rate
This age-dependent function has a declining logistic shape.
"""
function age_specific_hosp_rates(ageGroupSizes::Vector{Float},
                                 hospRateCutoffs::Vector{Float},
                                 hospRates::Vector{Float}
                                 ) :: Vector{Float}

    nAgeGroups = length(ageGroupSizes)
    nHospRates = length(hospRateCutoffs)

    hospRatesOfAgeGroups = zeros(Float, nAgeGroups)
    prevAge = 0.0
    ageGroupIdx = 0
    for hospRateIdx = 1 : nHospRates
        currentHospRate = hospRates[hospRateIdx]
        nextAgeCutoff = hospRateCutoffs[hospRateIdx]

        while ageGroupIdx < nAgeGroups &&
              (prevAge + ageGroupSizes[ageGroupIdx + 1]) <= nextAgeCutoff
            ageGroupIdx += 1
            hospRatesOfAgeGroups[ageGroupIdx] = currentHospRate
            prevAge += ageGroupSizes[ageGroupIdx]
        end
        if ageGroupIdx < nAgeGroups
            ageGroupIdx += 1
            newHospRate = hospRateIdx < nHospRates ?
                          hospRates[hospRateIdx + 1] : 0.0
            agePropInCurrHospRate = (nextAgeCutoff - prevAge) /
                                   ageGroupSizes[ageGroupIdx]
            hospRatesOfAgeGroups[ageGroupIdx] =
                agePropInCurrHospRate * currentHospRate +
                (1.0 - agePropInCurrHospRate) * newHospRate
            prevAge += ageGroupSizes[ageGroupIdx]
        else
            break
        end
    end
    return hospRatesOfAgeGroups
end # function


age_specific_hosp_rates(vars::ModelVars, params::ModelParams) =
    age_specific_hosp_rates(vars.ageGroupSizes, vars.hospRateCutoffs,
                            params.hospRates)


"""
    maternal_effects(ageGroupSizes::Vector{Float},
                     maternalProtectDuration::T,
                     adeAgeAvg::T,
                     adeAgeVar::T,
                     adeEffectScale::T,
                     nBinsPerAgeGroup::Int = 10) :: Vector{T}

Calculate the multiplicative age-dependent effect of the maternal immunity in
causing symptoms during a primary infection. This effect include 2 parts:
(1) When `age <= maternalProtectDuration`, the maternal immunity is fully
    protective, i.e. the effect scale is 0.
(2) When `age > maternalProtectDuration`, the maternal immunity can enhance
    infection. The enhancement effect (ADE) is modelled as a gaussian-shaped
    function, of which the maximum value is `adeEffectScale` and the minimum
    value is 1.0. This function reaches its maximum value when
    `age == aedAgeAvg`.

The ADE part is modelled by the following function:
    ADE(age) = exp( -((age - adeAgeAvg) ^ 2) / (2 * adeAgeVar) ) *
               (adeEffectScale - 1.0) + 1.0
"""
function maternal_effects(ageGroupSizes::Vector{Float},
                          maternalProtectDuration::Float,
                          adeAgeAvg::Float,
                          adeAgeVar::Float,
                          adeEffectScale::Float,
                          nBinsPerAgeGroup::Int = 10
                          ) :: Vector{Float}

    LOG_MINIMUM = -32  # ≈ ln(1e-14)

    nAgeGroups = length(ageGroupSizes)
    # if adeEffectScale <= 1.0
    #     return ones(Float, nAgeGroups)
    # end

    binSizes = ageGroupSizes / (nBinsPerAgeGroup + 1.0)
    age = 0.0
    maternalEffects = zeros(Float, nAgeGroups)
    for ageGroup = 1 : nAgeGroups
        age += binSizes[ageGroup]

        for bin = 1 : nBinsPerAgeGroup
            if age > maternalProtectDuration
                if adeEffectScale <= 1.0
                    maternalEffects[ageGroup] += 1.0
                else
                    logEffect = -((age - adeAgeAvg) ^ 2) / (2 * adeAgeVar)
                    if logEffect > LOG_MINIMUM
                        maternalEffects[ageGroup] +=
                            exp(logEffect) * (adeEffectScale - 1.0) + 1.0
                    else
                        maternalEffects[ageGroup] += 1.0
                    end
                end
            end
            age += binSizes[ageGroup]
        end
    end
    maternalEffects ./= nBinsPerAgeGroup

    return maternalEffects
end # function


maternal_effects(vars::ModelVars,
                 params::ModelParams,
                 nBinsPerAgeGroup::Int = 10) =
    maternal_effects(vars.ageGroupSizes,
                     params.maternalProtectDuration,
                     params.adeAgeAvg,
                     params.adeAgeVar,
                     params.adeEffectScale,
                     nBinsPerAgeGroup)





maternal_protective_effects(vars::ModelVars, params::ModelParams) =
    maternal_protective_effects(vars.ageGroupSizes,
                                params.maternalProtectDuration)



"""
Calculate the number of hospitalisations based on the given primary and
secondary incidences.
"""
function calculate_hospitalisations(vars::ModelVars,
                                    params::ModelParams,
                                    priIncidences::Matrix{Float},
                                    sndIncidences::Matrix{Float}
                                    )::Matrix{Float}

    maternalEffects = maternal_effects(vars, params)
    ageSpecHospRates = age_specific_hosp_rates(vars, params)

    priSymptomaticProb = params.priSymptomaticProb
    sndSymptomaticProb = params.sndSymptomaticProb

    hospitalisations =
        ( (priIncidences .* maternalEffects .* priSymptomaticProb) +
          (sndIncidences .* sndSymptomaticProb) ) .*
        ageSpecHospRates

    return hospitalisations
end


function calculate_hospitalisations(vars::ModelVars, params::ModelParams)
    odeInitState = DengueOdes.init_sys_state(vars, params)

    odeSolution = solve_odes(vars, params, odeInitState)

    priIncidences, sndIncidences =
        DengueOdes.compute_incidences(
            odeSolution,
            vars,
            params,
            vars.nDaysPerSummaryPeriod
        )
    return calculate_hospitalisations(vars,          params,
                                      priIncidences, sndIncidences)
end


end  # module DengueModels

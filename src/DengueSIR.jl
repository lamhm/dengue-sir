# using UnicodePlots
#
# Base.@ccallable function julia_main(ARGS::Vector{String})::Cint
#     println("hello, world")
#     @show sin(0.0)
#     println(lineplot(1:100, sin.(range(0, stop = 2π, length = 100))))
#     return 0
# end

module DengueSIR

include("globaldef.jl")

include("Util.jl")
include("ModelConfigs.jl")
include("HospitalData.jl")
include("DengueOdes.jl")
include("DengueModels.jl")
include("CustomDistributions.jl")

using .ModelConfigs
using .HospitalData
using .DengueOdes
using .DengueModels

export ModelVars
export ModelParams
export load_model_config

export load_data_from_csv
export as_3d_array

export init_sys_state
export redistribute_serotypes!
export extract_ode_class
export SIRColumnIndexer

export maternal_effects

export solve_odes
export compute_incidences
export compute_seroprevalence
export serialise_ode_sol
export deserialise_ode_sol

export calculate_seroprev_from_foi


import .CustomDistributions: FixedValDist
export FixedValDist


ModelVars         = ModelConfigs.ModelVars
ModelParams       = ModelConfigs.ModelParams
load_model_config = ModelConfigs.load_model_config

load_data_from_csv = HospitalData.load_data_from_csv
as_3d_array        = HospitalData.as_3d_array

init_sys_state          = DengueOdes.init_sys_state
redistribute_serotypes! = DengueOdes.redistribute_serotypes!
extract_ode_class       = DengueOdes.extract_ode_class
SIRColumnIndexer        = DengueOdes.SIRColumnIndexer

maternal_effects = DengueModels.maternal_effects

solve_odes             = DengueOdes.solve_odes
compute_incidences     = DengueOdes.compute_incidences
compute_seroprevalence = DengueOdes.compute_seroprevalence
serialise_ode_sol        = DengueOdes.serialise_ode_sol
deserialise_ode_sol      = DengueOdes.deserialise_ode_sol


function calculate_seroprev_from_foi(foi::Float,
                                     ageGroupSizes::Vector{Float},
                                     nDaysPerYear::Float)::Vector{Float}
    maxAges = cumsum(ageGroupSizes)
    avgAgesInYear =
        (maxAges - (ageGroupSizes ./ 2.0)) / nDaysPerYear
    seroprevalence = 1.0 .- exp.(-foi .* avgAgesInYear)
    return seroprevalence
end

calculate_seroprev_from_foi(modelVars::ModelVars) =
    calculate_seroprev_from_foi(modelVars.availFOI,
                                modelVars.ageGroupSizes,
                                modelVars.nDaysPerYear)

end  # module DengueSIR

module Util

include("globaldef.jl")


function group_matrix_columns(
            input::Matrix{T},
            nColumnsPerGroup::Int;
            maxConsecutiveZeros::Int = -1,
            missingMask::Real        = 0,
            rowBlocks::Vector{Vector{Int}} = Vector{Vector{Int}}(undef, 0)
            ) :: Matrix{T} where T<:Real

    nRows, nInputCols = size(input)
    nOutputCols = div(nInputCols, nColumnsPerGroup)

    if length(rowBlocks) <= 0
        rowBlocks = Vector{Vector{Int}}(undef, 1)
        rowBlocks[1] = [i for i = 1 : nRows]
    end

    output = zeros(T, nRows, nOutputCols)

    for outputCol = 1 : nOutputCols
        inputColRange = (
            (outputCol - 1) * nColumnsPerGroup + 1
            : outputCol * nColumnsPerGroup
        )

        for rowBlock in rowBlocks
            if length(rowBlock) <= 0
                continue
            end

            isDataMissing = false
            if 0 < maxConsecutiveZeros < nColumnsPerGroup
                # Try to detect if data is missing
                inputColSums = sum(input[rowBlock, inputColRange], dims = 1)
                nZerosFromLeft  = zeros(Int, nColumnsPerGroup)
                for i = 1 : nColumnsPerGroup
                    if inputColSums[i] <= 0
                        nZerosFromLeft[i] = 1
                        if i > 1
                            nZerosFromLeft[i] += nZerosFromLeft[i - 1]
                        end
                    end
                end
                nZerosFromRight = zeros(Int, nColumnsPerGroup)
                for i in nColumnsPerGroup : -1 : 1
                    if inputColSums[i] <= 0
                        nZerosFromRight[i] = 1
                        if i < nColumnsPerGroup
                            nZerosFromRight[i] += nZerosFromRight[i + 1]
                        end
                    end
                end
                nConsecutiveZeros = max(
                    maximum(nZerosFromLeft), maximum(nZerosFromRight)
                )
                if nConsecutiveZeros > maxConsecutiveZeros
                    isDataMissing = true
                end
            end

            if !isDataMissing
                output[rowBlock, outputCol] =
                    sum(input[rowBlock, inputColRange], dims = 2)
            else
                output[rowBlock, outputCol] =
                    [missingMask for i = 1 : length(rowBlock)]
            end
        end
    end

    return output
end # function group_matrix_columns



function zerofy_negative_numbers!(matrix::Matrix{T} where T<:Real)
    nRows, nCols = size(matrix)
    for row = 1 : nRows, col = 1 : nCols
        if matrix[row, col] < 0
            matrix[row, col] = 0
        end
    end
end



function movingAvg(series::Vector{T},
                   windowSize::Int,
                   movingWeight::Float = 1.0;
                   validateFunc::Function = !isnan)::Vector{Float} where T<:Real
    seriesLen = length(series)
    result = Vector{Float}(undef, seriesLen)
    halfSize = round(Int, (windowSize - 1.0) / 2.0, RoundNearestTiesUp)

    for i = 1 : seriesLen
        if !validateFunc(series[i])
            result[i] = series[i]
        else
            currentSum = series[i]
            nAvailElements = 1
            for k = 1 : halfSize
                currentWeight = movingWeight ^ k
                if (i + k <= seriesLen) && validateFunc(series[i + k])
                    currentSum += series[i + k] * currentWeight
                    nAvailElements += currentWeight
                end
                if (i - k >= 1) && validateFunc(series[i - k])
                    currentSum += series[i - k] * currentWeight
                    nAvailElements += currentWeight
                end
            end
            result[i] = Float(currentSum) / nAvailElements
        end
    end

    return result
end # function movingAvg


end  # module Util
